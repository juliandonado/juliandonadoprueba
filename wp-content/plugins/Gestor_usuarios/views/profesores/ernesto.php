<div class="jumbotron">
      <h1>Ernesto García</h1>  
</div>

    <center><img src="http://localhost/expofighting/wp-content/uploads/2020/03/ernesto2.png" class="ml-3 mt-3 rounded-circle" style="width:60%;"></center>

<p>Maestro en Música de la Universidad Sergio Arboleda.</p>
<h3>Ernesto adicionalmente tiene:</h3>

<ul>
    <li>Tiene mas de 10 años enseñando, ha trabajado con diferentes grupos de edad, y con poblaciones con necesidades especiales. </li>
    <li>Participa como guitarrista en Héctor & The Cast Band</li>
    <li>4 años como coach.</li>
    <li>Tecladista en la banda Tres Pasos.</li>
    <li>Se ha desempeñado en diferentes instrumentos, aunque su enfoque es en guitarra clásica.</li>
    <li>Trabaja en composición, arreglos e investigación, desarrollando una metodología que busca adaptarse a las características propias de cada alumno.</li>
</ul>
<div class="alert alert-info">
    <strong>Está al frente del proyecto</strong> Juan Manuel Romero, un pianista en condición de autismo (www.juanmanuelbarbosa.com.co)</a>.
  </div>