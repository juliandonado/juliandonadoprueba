<div class="container">
    <div class="jumbotron">
      <h1>Nuestro equipo de profesores</h1>      
      <p>En EXPOFIGHTING contamos con personal totalmente capacitado, con gran experiencia en nuestros programas, generando a nuestros usuarios satisfacción y seguridad.</p>
    </div>
    <div class="row">    
      <div class="col" >
      <h2>ING. Carlos Cárdenas</h2>
          <div class="card" style="width:400px">
          <center><img  src="http://localhost/expofighting/wp-content/uploads/2020/02/carlos.jpg" class="ml-3 mt-3 rounded-circle" style="width:60%;" alt="Carlos Cardénas" style="width:50%"></center>
            <div class="card-body">
              <p class="card-text">Ingeniero Industrial y especialista en terapia ocupacional</p>
              <a href="http://localhost/expofighting/ingeniero-carlos-cardenas/" class="btn btn-primary stretched-link">Ver mas...</a>
            </div>  
          </div>
        </div>
      <div class="col" >
        <h2>Jairo Mahecha</h2>
          <div class="card" style="width:400px">
          <center><img  src="http://localhost/expofighting/wp-content/uploads/2020/03/jairo.jpg" class="ml-3 mt-3 rounded-circle" style="width:60%;" alt="Jairo Mahecha" style="width:50%"></center>
          <div class="card-body">
            <p class="card-text">Artista marcial con más de 38 años  de práctica</p>
            <a href="http://localhost/expofighting/jairo-mahecha/" class="btn btn-primary stretched-link">Ver mas...</a>
          </div>    
        </div> 
      </div> 
    <div class="col" >
      <h2>Daniela Hidalgo</h2>
        <div class="card" style="width:400px">
        <center><img  src="http://localhost/expofighting/wp-content/uploads/2020/03/Daniela.jpg" class="ml-3 mt-3 rounded-circle" style="width:60%;" alt="Daniela Hidalgo" style="width:50%"></center>
        <div class="card-body">
          <p class="card-text">Licenciada en educación física (PC-LEF) de la Universidad Pedagógica Nacional de Colombia</p>
          <a href="http://localhost/expofighting/daniela-hidalgo/" class="btn btn-primary stretched-link">Ver mas...</a>
        </div>    
      </div>
    </div> 
    <div class="col" >
      <h2>Jean Marcelo Rios</h2>
        <div class="card" style="width:400px">
        <center><img src="http://localhost/expofighting/wp-content/uploads/2020/03/Marcelo.jpg" class="ml-3 mt-3 rounded-circle" style="width:60%;" alt="Jean marcelo Rios" style="width:50%"></center>
        <div class="card-body">
          <p class="card-text">Profesional en Cultura Física, Deporte y Recreación, Universidad Santo Tomás</p>
          <a href="http://localhost/expofighting/jean-marcelo-rios/" class="btn btn-primary stretched-link">Ver mas...</a>
        </div>    
      </div> 
    </div>
    <div class="col" >
      <h2>Ernesto García</h2>
        <div class="card" style="width:400px">
        <center><img  src="http://localhost/expofighting/wp-content/uploads/2020/03/ernesto2.png" class="ml-3 mt-3 rounded-circle" style="width:60%;" alt="Ernesto García" style="width:50%"></center>
        <div class="card-body">
          <p class="card-text">Maestro en Música de la Universidad Sergio Arboleda.</p>
          <a href="http://localhost/expofighting/ernesto-garcia/" class="btn btn-primary stretched-link">Ver mas...</a>
        </div>    
      </div> 
    </div>
  </div> 
</div>



