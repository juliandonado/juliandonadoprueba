<div class="jumbotron text-center">
  <h1>PROGRAMAS</h1>
  <p>En nuestro centro deportivo tenemos varios programas para las diferentes necesidades. Planes para tú y tu familia. </p> 
</div>


<div class="container">
  <div class="row">
    <div class="col-sm-3">
      <h3>Kickboxing</h3>
      <img src="http://localhost/expofighting/wp-content/uploads/2020/02/kick-boxing-deportivo-9.jpg" class="img-rounded" alt="" width="204" height="136"> 
      <p><strong>EJCUTIVO:</strong> Dortalece y manteniene tu cuerpo saludable</p>
      <p><strong>DEPORTIVO:</strong> Preparación para competencias y torneos</p>
      <a href="http://localhost/expofighting/kickboxing/" class="btn btn-info" role="button">Ver mas...</a>
    </div>
    <div class="col-sm-3">
      <h3>Taekwondo</h3>
      <img src="http://localhost/expofighting/wp-content/uploads/2020/02/taekwondo-portivo-5.jpg"class="img-rounded" alt="" width="204" height="136"> 
      <p><strong>INFANTIL:</strong> Iniciación deportiva para tus hijos con profesionales calificados.</p>
      <p><strong>DEPORTIVO:</strong> Mejoramiento y efectividad en los movimientos gospes y técnica.</p>
      <a href="http://localhost/expofighting/taekwondo/" class="btn btn-info" role="button">Ver mas...</a>
    </div>
    <div class="col-sm-3">
      <h3>Exploración Motriz</h3> 
      <img src="http://localhost/expofighting/wp-content/uploads/2020/02/logo-completomini.png" class="img-rounded" alt="" width="204" height="136">        
      <p>Programa dirigido a niños desde los 2.5 a 6 años de edad</p>
      <a href="http://localhost/expofighting/exploracion-motriz/" class="btn btn-info" role="button">Ver mas...</a>
    </div>
    <div class="col-sm-3">
      <h3>Música</h3>      
      <img src="http://localhost/expofighting/wp-content/uploads/2020/02/kick-boxing-deportivo-9.jpg" class="img-rounded" alt="" width="204" height="136">   
      <p><strong>EXPLORACIÓN MUSICAL:</strong> Buscamos desarrollar en los niños habilidades de escucha, concentración y disciplina.
      <p><strong>MÚSICA:</strong> Desarrollo a nivel instrumental que construya elementos de ritmo, afinación, disociación y trabajo grupal.</p>
      <a href="http://localhost/expofighting/musica/" class="btn btn-info" role="button">Ver mas...</a>
    </div>
  </div>
</div>
