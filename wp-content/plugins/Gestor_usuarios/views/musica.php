
<div class="jumbotron text-center">
  <h1>Música</h1>
  <p>Contamos con estudiantes de diferentes edades que están encaminados al aprendizaje instrumental, necesario para el desarrollo de los elementos teóricos de la música.</p> 
</div>

<h1>Exploración Musical</h1>

<div class="alert alert-primary">
    <strong>Grupo de iniciación musical!</strong> Niños hasta los 6 años de edad.
</div>

<p>Buscamos desarrollar en los niños habilidades de escucha, concentración y disciplina por medio de juegos, en este espacio tienen la oportunidad de conocer otras personas y trabajar en equipo fortaleciendo la confianza en si mismo mientras adquieren habilidades musicales.
(motricidad fina, ritmo, concentración, )</p>

<p>El programa se orienta a desarrollar inicialmente las capacidades de atención, comprensión de instrucciones básicas en la música, canto, desarrollo de la sensación de ritmo y melodía, a partir de diferentes metodologías pedagógicas que se aplican especialmente a este grupo de edad.</p>

<h1>Música</h1>

<div class="alert alert-primary">
    <strong>Grupo de Música!</strong> Alumnos mayores de 6 años.
</div>

<p>
Nos enfocamos en un aprendizaje basado en la sensibilidad, apreciación y el acercamiento perceptivo a la música, se construye un desarrollo a nivel instrumental que construya elementos de ritmo, afinación, disociación y trabajo grupal. El contenido teórico será comprendido  de una manera eficiente, pues la práctica ya ha desarrollado habilidades que ayudan a concebir los conceptos nuevos en el proceso de aprendizaje.</p>


<h1>Horarios</h1>

<div class="alert alert-primary">
    <strong>Atención!</strong> Las clase se realizan entre senama de forma continua desde las 4 hasta las 8pm.
</div>

<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>LUNES</th>
            <th>MARTES</th>
            <th>MIÉRCOLES</th>
            <th>JUEVES</th>
            <th>VIERNES</th>
            <th>SABADO</th>
            <th>DOMINGO</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>4:00 pm</td>
            <td></td>
            <td>4:00 pm</td>
            <td></td>
            <td></td>
            <td>9:00 am</td>
            <td>9:00 am</td>
        </tr>
        </tbody>
    </table>
</div>

<h1>Galería de imágenes</h1>
<?php
echo do_shortcode('[smartslider3 slider=2]');
?>
Todos los horarios:
Lunes y Miercoles  Desde las 4:00 pm
Sabado desde las 9:00 am
Domingo desde las 9:00 am

