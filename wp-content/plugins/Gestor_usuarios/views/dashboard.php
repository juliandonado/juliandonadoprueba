<script>

</script>

<div class="container">
  <div class="jumbotron">
    <h1>

        <svg class="bi bi-gear-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 01-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 01.872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 012.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 012.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 01.872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 01-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 01-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 100-5.86 2.929 2.929 0 000 5.858z" clip-rule="evenodd"/>
        </svg>
    
    CENTRO DE ADMINISTRACIÓN DE EXPOFIGHTING</h1>      
    <p>Centro de configuración, seguimiento y gestión de EXPOFIGHTING. Desde ete panel podrá gestionar usuarios (Creación y edición), podrá también cambiar el valor de la mensualidad y la matricula, podrá ver y gesionar programas, inscripciones, adicionalmnete tendrá una lista de las personas que están cumpliendo años en el mes actual y realizar seguimiento a las personas que estén en mora según su fecha de seguimiento. </p>
  </div>

  <center> 
    <div class="container">
        <div class="container-fluid">
            <div class="d-inline-flex p-3 bg-secondary text-white">  
                <div class="p-2 bg-info">
                    <h1>
                    <svg class="bi bi-people-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
                    </svg>
                    Gestionar usuarios </h1>
                    <p>En esta sección podrá inscribir y modificar usuarios.</p>
                </div>
            </div>
        </div>
    </div>
</center> 
<br><br>

<div class="container-fluid">
  <div class="row">
    <div class="col" ><a href="http://localhost/expofighting/inscribir-usuario/" class="btn btn-primary">
        <svg class="bi bi-person-plus-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm7.5-3a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
        <path fill-rule="evenodd" d="M13 7.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0v-2z"/>
        </svg> Incribir usuario</a>
    </div>
    <div class="col" ><a href="http://localhost/expofighting/actualizar-usuario/" class="btn btn-primary">
        <svg class="bi bi-person-check-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm9.854-2.854a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L12.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
        </svg> Actualizar usuario</a>
    </div>
    <div class="col" ><a href="http://localhost/expofighting/listar-usuarios/" class="btn btn-primary">
        <svg class="bi bi-person-lines-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm7 1.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm2 9a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>
        </svg> Ver usuarios creados</a>
    </div>
  </div>
</div>

<br><br>

<div class="container">
  <h2>Total de usuarios actuales</h2>
  <table class="table table-bordered">
    <thead style="text-align:center;">
      <tr>
        <th>Usuarios activos</th>
        <th>Usuarios inactivos</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody style="text-align:center ;">
    <?php 

       GLOBAL $wpdb;

       $result_active=0;
       $result_inactive=0;

        foreach($result_user_active as $user_active){
            

            if($user_active->e_pk==1){

                $result_active++;
            
                
            }

            if($user_active->e_pk==2){

                $result_inactive++;

            }
            

        }

        ?>
      <tr>
        <td style="color:blue;"><?php echo $result_active; ?></td>
        <td style="color:blue;"><?php echo $result_inactive; ?></td>
        <td style="color:red; font-weight: bold;"><?php $suma_estados= $result_inactive+$result_active; echo $suma_estados; ?></td>
      </tr>
    </tbody>
  </table>
</div>

<br><br>

<center> 
    <div class="container">
        <div class="container-fluid">
            <div class="d-inline-flex p-3 bg-secondary text-white">  
                <div class="p-2 bg-info">
                    <h1>
                    <svg class="bi bi-briefcase-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M0 12.5A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5V6.85L8.129 8.947a.5.5 0 0 1-.258 0L0 6.85v5.65z"/>
                        <path fill-rule="evenodd" d="M0 4.5A1.5 1.5 0 0 1 1.5 3h13A1.5 1.5 0 0 1 16 4.5v1.384l-7.614 2.03a1.5 1.5 0 0 1-.772 0L0 5.884V4.5zm5-2A1.5 1.5 0 0 1 6.5 1h3A1.5 1.5 0 0 1 11 2.5V3h-1v-.5a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5V3H5v-.5z"/>
                    </svg>
                    Valor Mensualidad y matrícula  <STRONG>ACTUALES</STRONG></h1>
                    <p>En esta sección podrá ver y modificar los valores de la matrícula y la mensualidad.</p>
                </div>
            </div>
        </div>
    </div>
</center> 
<br><br>

<div class="container-fluid">
    <div class="row">
        <div class="col" >
            <br>
            <h3 style="text-align: center;">Mensualidad 
                <div class="container">
                    <!-- Button to Open the Modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mensualidad">
                    <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                    </svg> Editar
                    </button>
                </div>
            </h3>            
            <table class="table table-hover table-bordered " style="text-align: center;">
                <thead>
                    <tr>
                        <th>Valor actual</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href=""><strong><?php foreach($mensualidad_actual as $precio){ 
                            echo "$".$precio->m_valor;  } ?></strong></a></td>
                    </tr>
                </tbody>
            </table>

        </div>

        <div class="col" >
            <br>
            <h3 style="text-align: center;">Matricula 
                <div class="container">
                    <!-- Button to Open the Modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                    </svg> Editar
                    </button>
                </div>
            </h3>            
            <table class="table table-hover table-bordered " style="text-align: center;">
                <thead>
                    <tr>
                        <th>Valor actual</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href=""><strong><?php foreach($result_valor_Actual as $precio){
                             echo "$".$precio->ma_valor;  } ?></strong></a></td>
                    </tr>
                </tbody>
            </table>
        </div>
</div>





  <!-- The Modal Mensualidad-->
  <div class="modal" id="mensualidad">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Editar Mensualidad</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
            <div class="jumbotron">
                <h1>MODIFICAR MENSUALIDAD</h1>      
                <p>Modifique el valor de la mensualidad para todos los usuarios</p>
            </div>

            <div class="container">
                <h2>Editar mensualidad</h2>
                <div class="alert alert-info">
                        <strong>Mensualidad actual:</strong> <?php foreach($mensualidad_actual as $precio){ echo "$".$precio->m_valor;  } ?>
                    </div>

                <form action="controller/controlador.php?metodo=mensualidad" name="mensualidad_form" method="POST" novalidate>

                    <label  class="mb-2 mr-sm-2"> Ingrese el nuevo valor de la mensualidad:</label>
                    <input type="text"  placeholder="Nuevo Valor" name="new_value">
                    <button type="submit" class="btn btn-primary">
                    <svg class="bi bi-arrow-clockwise" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M3.17 6.706a5 5 0 017.103-3.16.5.5 0 10.454-.892A6 6 0 1013.455 5.5a.5.5 0 00-.91.417 5 5 0 11-9.375.789z" clip-rule="evenodd"/>
                        <path fill-rule="evenodd" d="M8.147.146a.5.5 0 01.707 0l2.5 2.5a.5.5 0 010 .708l-2.5 2.5a.5.5 0 11-.707-.708L10.293 3 8.147.854a.5.5 0 010-.708z" clip-rule="evenodd"/>
                    </svg>
                Actualizar mensualidad</button>
                                                    
                </form>
            </div>


        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
</div>

 <!-- The Modal Matricula-->
 <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Editar Matrícula</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div class="jumbotron">
                    <h1>MODIFICAR MATRICULA</h1>      
                    <p>Modifique el valor de la matricula para todos los usuarios</p>
                </div>

                <div class="container">
                <h2>Editar matricula</h2>
                <div class="alert alert-info">
                        <strong>Matricula actual:</strong> <?php foreach($result_valor_Actual as $precio){ echo "$".$precio->ma_valor;  } ?>
                    </div>

                <form action="controller/controlador.php?metodo=matricula" name="matricula_form" method="POST" novalidate>

                    <label> Ingrese el nuevo valor de la matricula.:</label>
                    <input type="text"  placeholder="Nuevo Valor" name="new_matricula">
                    <button type="submit" class="btn btn-primary">Actualizar matricula</button>
                                                    
                </form>
                </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
</div>


<br><br>

<center> 
    <div class="container">
        <div class="container-fluid">
            <div class="d-inline-flex p-3 bg-secondary text-white">  
                <div class="p-2 bg-info">
                    <h1>
                    <svg class="bi bi-briefcase-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M0 12.5A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5V6.85L8.129 8.947a.5.5 0 0 1-.258 0L0 6.85v5.65z"/>
                        <path fill-rule="evenodd" d="M0 4.5A1.5 1.5 0 0 1 1.5 3h13A1.5 1.5 0 0 1 16 4.5v1.384l-7.614 2.03a1.5 1.5 0 0 1-.772 0L0 5.884V4.5zm5-2A1.5 1.5 0 0 1 6.5 1h3A1.5 1.5 0 0 1 11 2.5V3h-1v-.5a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5V3H5v-.5z"/>
                    </svg>
                    Paquetes actuales <STRONG>ACTIVOS</STRONG></h1>
                    <p>Acá se listan los paquetes que se han creado con la descripción. Podrá también editarlos y crear paquetes adicionales.</p>
                </div>
            </div>
        </div>
    </div>
</center> 
<br><br>

<div class="container">
    <div class="container-fluid">
    <div class="row">
        <div class="col">
            <!-- Button to Open the Modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#admin_paqts">
                <svg class="bi bi-bag-plus" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V5zM1 4v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4H1z"/>
                <path d="M8 1.5A2.5 2.5 0 0 0 5.5 4h-1a3.5 3.5 0 1 1 7 0h-1A2.5 2.5 0 0 0 8 1.5z"/>
                <path fill-rule="evenodd" d="M8 7.5a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5H6a.5.5 0 0 1 0-1h1.5V8a.5.5 0 0 1 .5-.5z"/>
                <path fill-rule="evenodd" d="M7.5 10a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0v-2z"/>
                </svg> Crear paquete
            </button>
        
        </div>
        <div class="col">
            <!-- Button to Open the Modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#activar_paquetes">
                <svg class="bi bi-bag-plus" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V5zM1 4v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4H1z"/>
                <path d="M8 1.5A2.5 2.5 0 0 0 5.5 4h-1a3.5 3.5 0 1 1 7 0h-1A2.5 2.5 0 0 0 8 1.5z"/>
                <path fill-rule="evenodd" d="M8 7.5a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5H6a.5.5 0 0 1 0-1h1.5V8a.5.5 0 0 1 .5-.5z"/>
                <path fill-rule="evenodd" d="M7.5 10a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0v-2z"/>
                </svg> Activar paquetes
            </button>
        </div>
    </div>
    </div>
 
  
  <br><br>



  <!-- The Modal -->
  <div class="modal fade" id="admin_paqts">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Crear nuevo paquete de servicios.</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <form action="controller/controlador.php?metodo=new_paquete" name="nuevo_paquete" method="POST" novalidate>

            <div class="container">
                <div class="jumbotron">
                    <h1>Crear nuevo Paquete</h1>      
                    <p>Desde esta opción puede crear un nuevo paquete desde 0.</p>
                </div>  
            </div>

                <div class="form-group">

                    <h5>Ingresar nuevo nombre del paquete</h5>
                    
                    <label for="usr">Nombre:</label>
                    <input type="text" class="form-control" name="nombre_pqt" placeholder="Nombre del Paquete nuevo">
                    <br><br>

                    <label for="usr">Ingrese el ID:</label>
                    <div class="alert alert-warning">
                        <strong>Atención!</strong> El ID debe ser numérico y no debe existir en la base de datos.
                    </div>
                    <input type="number" class="form-control" name="id_paquete" placeholder="Ingrese el ID del paquete">
                    <br><br>
                
                    <h5>Ingresar nueva descripción</h5>

                    <div class="alert alert-danger">
                        <strong>Atención!</strong> Tenga en cuenta que está descripción puede publicarse en la página y podrá ser vista por los usuarios en general, Por tanto es importante dejarlo correctamente digitado y estructurado. 
                    </div>
                
                    <label for="usr">Contenido de la descipción Pública</label>
                    <textarea name="description"  cols="90" rows="8"></textarea>

                    <h5>Comentario interno</h5>

                    <div class="alert alert-warning">
                        <strong>Atención!</strong> Tenga en cuenta que este comentario será manejado de forma interna y no se verá publicado al público en general. Aca podrá dejar las aclaraciones que crea conveniente de manera interna. 
                    </div>

                    <label for="usr">Comentario adicional interno del paquete:</label>
                    <textarea name="comentario"  cols="90" rows="8"></textarea>
<br><br>
                    <label for="usr">Selecciones estado con el que desea crear el paquete:</label> 
                    <br><br>

                    <select name="estado_paquete" class="form-control">
                        <option value="su">SELECCIONE UNO</option>
                            <?php
                                foreach($result_estados as $estado_new_paque){
                                    $pk_nuevo_estado=$estado_new_paque->e_pk;
                                    $nombre_estado=$estado_new_paque->e_nombre;

                                    echo '<option value="'.$pk_nuevo_estado.'">'.$nombre_estado.'</option>';
                                }
                            ?>
                    </select>

                    
                </div>
                <button type="submit" class="btn btn-primary">
                    Crear nuevo paquete
                </button>
            </form>
        
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar ventana</button>
        </div>
        
      </div>
    </div>
  </div>
  
</div>







<!-- The Modal activar pquetes-->
<div class="modal fade" id="activar_paquetes">
    <div class="modal-dialog modal-lg ">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Activar paquetes de servicios.</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div class="container">
                <div class="jumbotron">
                    <h1><svg class="bi bi-check-circle-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                  </svg> Ver y activar paquetes inactivos</h1>      
                    <p>Desde esta parte puede ver y activar los paquetes que requiera.</p>
                </div>  
            </div>
            
            <form action="controller/controlador.php?metodo=activar_paquete" name="activar_paquete" method="POST" novalidate>
            
                   

            $tabla_paquetes_inactivo= '

            <table class="table table-dark table-hover table-bordered table-sm table-responsive">
                <thead>
                <tr>
                    <th>Paquete</th>
                    <th>ID</th>
                    <th>Detalle</th>
                    <th>Comentario</th>
                    <th>Fecha de creación</th>
                    <th>Activar <svg class="bi bi-check-circle-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                  </svg></th>
                </tr>
                </thead>
                <tbody>
                <tr>';

                <?php
                GLOBAL $wpdb;

                foreach($result_paquetes as $pq_inactivo){

                    $nombre=$pq_inactivo->pq_nombre;
                    $id=$pq_inactivo->pq_id;
                    $detalle=$pq_inactivo->pq_descripción;
                    $comment=$pq_inactivo->pq_comentario;
                    $fecha=$pq_inactivo->pq_fecha_creacion;
                    $pk=$pq_inactivo->pq_pk;
                    
                    echo 
                   '<td>'.$nombre .'</td>
                    <td>'.$id .'</td>
                    <td>'.$detalle .'</td>
                    <td>'.$comment .'</td>
                    <td>'.$fecha.'</td>
                    <td style="text-align:center; position: relative; margin-top: -0.2rem; margin-left: -1.25rem;">  
                        <div class="form-check">
                            <label class="form-check-label" >
                                <input type="radio" class="form-check-input" 
                                name="activar[]" value="'.$pk.'">';
                            } ?>
                            </label>
                        </div>
                    </td>
                    
                </tr>
                
                </tbody>
            </table>
                <button type="submit" class="btn btn-primary">Activar</button>
            </form>
        
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar ventana</button>
        </div>
        
      </div>
    </div>
  </div>
  
</div>
            <table class="table table-hover table-bordered table-sm table-responsive">
                <thead>
                
                    <tr>
                        <?php
                            foreach($result_paquete1 as $paquete_01){

                                $nombrepq=$paquete_01->pq_nombre;

                                if($paquete_01==1){

                                echo'

                                <th>'.$nombrepq.'<br>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#paquete1">
                                    Editar
                                        <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                </th>';

                                }
                            }

                        ?>
                        <?php
                            foreach($result_paquete2 as $paquete_02){

                                $nombrepq=$paquete_02->pq_nombre;

                                if($paquete_02==1){

                                echo'

                                <th>'.$nombrepq.'<br>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#paquete2">
                                    Editar
                                        <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                </th>';

                                }
                            }

                        ?>
                        <?php
                            foreach($result_paquete3 as $paquete_03){

                                $nombrepq=$paquete_03->pq_nombre;

                                if($paquete_03==1){

                                echo'

                                <th>'.$nombrepq.'<br>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#paquete3">
                                    Editar
                                        <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                </th>';

                                }
                            }

                        ?>
                        <?php
                            foreach($result_paquete4 as $paquete_04){

                                $nombrepq=$paquete_04->pq_nombre;

                                if($paquete_04==1){

                                echo'

                                <th>'.$nombrepq.'<br>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#paquete4">
                                    Editar
                                        <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                </th>';

                                }
                            }

                        ?>
                        <?php
                            foreach($result_paquete5 as $paquete_05){

                                $nombrepq=$paquete_05->pq_nombre;

                                if($paquete_05==1){

                                echo'

                                <th>'.$nombrepq.'<br>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#paquete5">
                                    Editar
                                        <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                </th>';

                                }
                            }

                        ?>
                        <?php
                            foreach($result_paquete6 as $paquete_06){

                                $nombrepq=$paquete_06->pq_nombre;

                                if($paquete_06==1){

                                echo'

                                <th>'.$nombrepq.'<br>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#paquete6">
                                    Editar
                                        <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                </th>';

                                }
                            }

                        ?>
                        <?php
                            foreach($result_paquete7 as $paquete_07){

                                $nombrepq=$paquete_07->pq_nombre;

                                if($paquete_07==1){

                                echo'

                                <th>'.$nombrepq.'<br>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#paquete7">
                                    Editar
                                        <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                </th>';

                                }
                            }

                        ?>
                        <?php
                            foreach($result_paquete8 as $paquete_08){

                                $nombrepq=$paquete_08->pq_nombre;

                                if($paquete_08==1){

                                echo'

                                <th>'.$nombrepq.'<br>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#paquete8">
                                    Editar
                                        <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                </th>';

                                }
                            }

                        ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <?php  GLOBAL $wpdb ; 

                        foreach($result_paquete1 as $pq_1){

                            $pq_descripcion=$pq_1->pq_descripción;
                            $pq_activo=$pq_1->e_pk;
                            $pq_id=$pq_1->pq_id;

                            if($pq_activo==1){
                            echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                    }

                        foreach($result_paquete2 as $pq_2){

                            $pq_descripcion=$pq_2->pq_descripción;
                            $pq_activo=$pq_2->e_pk;
                            $pq_id=$pq_2->pq_id;

                            if($pq_activo==1){
                            echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            }
                    
                        foreach($result_paquete3 as $pq_3){

                                $pq_descripcion=$pq_3->pq_descripción;
                                $pq_id=$pq_3->pq_id;
                                $pq_activo=$pq_3->e_pk;

                                if($pq_activo==1){
                                    echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            } 

                        foreach($result_paquete4 as $pq_4){

                            $pq_descripcion=$pq_4->pq_descripción;
                            $pq_id=$pq_4->pq_id;
                            $pq_activo=$pq_4->e_pk;

                            if($pq_activo==1){
                                echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            } 
                        
                        foreach($result_paquete5 as $pq_5){

                            $pq_descripcion=$pq_5->pq_descripción;
                            $pq_id=$pq_5->pq_id;
                            $pq_activo=$pq_5->e_pk;

                            if($pq_activo==1){
                                echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            } 
                            
                        foreach($result_paquete6 as $pq_6){

                            $pq_descripcion=$pq_6->pq_descripción;
                            $pq_id=$pq_6->pq_id;
                            $pq_activo=$pq_6->e_pk;

                            if($pq_activo==1){
                                echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            } 
                            
                        foreach($result_paquete7 as $pq_7){
                            
                            $pq_descripcion=$pq_7->pq_descripción;
                            $pq_id=$pq_7->pq_id;
                            $pq_activo=$pq_7->e_pk;

                            if($pq_activo==1){
                                echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            }  
                        
                        foreach($result_paquete8 as $pq_8){

                            $pq_descripcion=$pq_8->pq_descripción;
                            $pq_id=$pq_8->pq_id;
                            $pq_activo=$pq_8->e_pk;

                            if($pq_activo==1){
                                echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            } ?>                          
                    </tr>
                </tbody>
            </table>

             <!-- The Modal -->
             <div class="modal" id="paquete1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <div class="alert alert-dark">
                                    <strong>Editar el paquete: "<?php foreach($result_paquete1 as $pq_name){
                                        echo $pq_name->pq_nombre; }?>"</strong>
                                </div>
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;
                            </button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">

                            <form action="controller/controlador.php?metodo=edit_paquete" name="matricula_form" method="POST" >

                            <input type="hidden" name="pk_paquete"  value="<?php GLOBAL $wpdb ; foreach($result_paquete1 as $pq_1){  $pk_paquete=$pq_1->pq_pk; echo $pk_paquete; } ?>">

                            <h5>ID del plan: <?php echo $pq_1->pq_id; ?></h5>

                            <h2>Cambiar el nombre del plan</h2>

                            <input type="text" name="pq_nombre" class="form-control"  placeholder="Ingrese el nuevo nombre del paquete">
                            <br><br>
                            <h2>Descripción actual del plan</h2>

                            <p>
                                    
                                <div class="alert alert-info">
                                <strong><?php foreach($result_paquete1 as $pq_1){ 
                                echo $pq_1->pq_descripción; ?></strong>
                                </div>
                                
                            </p>
                               
                                <h5>Ingresar nueva descripción</h5>
                               
                                <div class="alert alert-warning">
                                    <strong>Atención!</strong> Tenga en cuenta que está descripción puede publicarse en la página y podrá ser vista por los usuarios en general, Por tanto es importante dejarlo correctamente digitado y estructurado. 
                                </div>

                                <br>
                                <textarea name="new_description"  cols="90" rows="8" class="form-control"></textarea>
                                <br>
                                <br>
                                
                                <h2>Comentario interno</h2>

                                <p>
                                <div class="alert alert-info">
                                    <strong><?php echo $pq_1->pq_comentario_interno; ?></strong>
                                </div>
                            <?php  } ?>
                            </p>

                            <h5>Actualice el nuevo comentario interno.</h5>
                                
                                <div class="alert alert-danger">
                                    <strong>Atención!</strong> Tenga en cuenta que este comentario será manejado de forma interna y no se verá publicado al público en general. Aca podrá dejar las aclaraciones que crea conveniente de manera interna. 
                                </div>

                                <br>
                                <textarea name="new_comentario"  cols="90" rows="8" class="form-control"></textarea>
                                <br>
                                
                                <br>
                                
                                <h2>
                                Estado Actual
                                </h2>
                                <p>
                                    <?php foreach($result_paquete1 as $old_estado){
                                        echo  '<div class="alert alert-info">
                                        <strong>'.$old_estado->e_nombre.'</strong> Este paquete está disponible para ser procesado en las matriculas y pensión de los usuarios y para ser publicada en la página de requerirse.
                                        </div>';
                                    }?>
                                </p>

                                <br><br>

                                <h5>Inactivar paquete.</h5>

                                <select name="cambiar_estado" class="form-control" required>
                                    <option value="su">Seleccione uno</option>

                                    <?php 
                                        
                                        foreach($result_estados as $estado_new_paque){
                                        $nombre_estado=$estado_new_paque->e_nombre;
                                        $pk_nuevo_estado=$estado_new_paque->e_pk;

                                        echo '<option value="'.$pk_nuevo_estado.'">'.$nombre_estado.'</option>';
                                    }
                                    ?>

                                </select>
                               
                                <br>
                                <br>

                                <button type="submit" class="btn btn-primary">
                                    Actualizar
                                </button>
                            </form>
                            
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal PAQUETE 1-->

            <!-- The Modal -->
            <div class="modal" id="paquete2">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                        <h4 class="modal-title">
                                <div class="alert alert-dark">
                                    <strong>Editar el paquete: "<?php foreach($result_paquete2 as $pq_name){
                                        echo $pq_name->pq_nombre; }?>"</strong>
                                </div>
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;
                            </button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">

                        <form action="controller/controlador.php?metodo=edit_paquete" name="matricula_form" method="POST" >

                        <input type="hidden" name="pk_paquete"  value="<?php GLOBAL $wpdb ; foreach($result_paquete2 as $pq_2){  $pk_paquete=$pq_2->pq_pk; echo $pk_paquete; } ?>">

                        <h5>ID del plan: <?php echo $pq_2->pq_id; ?></h5>

                        <h2>Cambiar el nombre del plan</h2>

                        <input type="text" name="pq_nombre" class="form-control"  placeholder="Ingrese el nuevo nombre del paquete">
                        <br><br>
                        <h2>Descripción actual del plan</h2>

                        <p>
                                
                            <div class="alert alert-info">
                            <strong><?php foreach($result_paquete2 as $pq_2){ 
                            echo $pq_2->pq_descripción; ?></strong>
                            </div>
                            
                        </p>
                        
                            <h5>Ingresar nueva descripción</h5>
                        
                            <div class="alert alert-warning">
                                <strong>Atención!</strong> Tenga en cuenta que está descripción puede publicarse en la página y podrá ser vista por los usuarios en general, Por tanto es importante dejarlo correctamente digitado y estructurado. 
                            </div>

                            <br>
                            <textarea name="new_description"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            <br>
                            
                            <h2>Comentario interno</h2>

                            <p>
                            <div class="alert alert-info">
                                <strong><?php echo $pq_2->pq_comentario_interno; ?></strong>
                            </div>
                        <?php  } ?>
                        </p>

                        <h5>Actualice el nuevo comentario interno.</h5>
                            
                            <div class="alert alert-danger">
                                <strong>Atención!</strong> Tenga en cuenta que este comentario será manejado de forma interna y no se verá publicado al público en general. Aca podrá dejar las aclaraciones que crea conveniente de manera interna. 
                            </div>

                            <br>
                            <textarea name="new_comentario"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            
                            <br>
                            
                            <h2>
                            Estado Actual
                            </h2>
                            <p>
                                <?php foreach($result_paquete1 as $old_estado){
                                    echo  '<div class="alert alert-info">
                                    <strong>'.$old_estado->e_nombre.'</strong> Este paquete está disponible para ser procesado en las matriculas y pensión de los usuarios y para ser publicada en la página de requerirse.
                                    </div>';
                                }?>
                            </p>

                            <br><br>

                            <h5>Inactivar paquete.</h5>

                            <select name="cambiar_estado" class="form-control" required>
                                <option value="su">Seleccione uno</option>

                                <?php 
                                    
                                    foreach($result_estados as $estado_new_paque){
                                    $nombre_estado=$estado_new_paque->e_nombre;
                                    $pk_nuevo_estado=$estado_new_paque->e_pk;

                                    echo '<option value="'.$pk_nuevo_estado.'">'.$nombre_estado.'</option>';
                                }
                                ?>

                            </select>
                        
                            <br>
                            <br>

                            <button type="submit" class="btn btn-primary">
                                Actualizar
                            </button>
                        </form>

                            
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->

            <!-- The Modal -->
            <div class="modal" id="paquete3">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                        <h4 class="modal-title">
                                <div class="alert alert-dark">
                                    <strong>Editar el paquete: "<?php foreach($result_paquete3 as $pq_name){
                                        echo $pq_name->pq_nombre; }?>"</strong>
                                </div>
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;
                            </button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">

                        <form action="controller/controlador.php?metodo=edit_paquete" name="matricula_form" method="POST" >

                        <input type="hidden" name="pk_paquete"  value="<?php GLOBAL $wpdb ; foreach($result_paquete3 as $pq_3){  $pk_paquete=$pq_3->pq_pk; echo $pk_paquete; } ?>">

                        <h5>ID del plan: <?php echo $pq_3->pq_id; ?></h5>

                        <h2>Cambiar el nombre del plan</h2>

                        <input type="text" name="pq_nombre" class="form-control"  placeholder="Ingrese el nuevo nombre del paquete">
                        <br><br>
                        <h2>Descripción actual del plan</h2>

                        <p>
                                
                            <div class="alert alert-info">
                            <strong><?php foreach($result_paquete3 as $pq_3){ 
                            echo $pq_3->pq_descripción; ?></strong>
                            </div>
                            
                        </p>
                        
                            <h5>Ingresar nueva descripción</h5>
                        
                            <div class="alert alert-warning">
                                <strong>Atención!</strong> Tenga en cuenta que está descripción puede publicarse en la página y podrá ser vista por los usuarios en general, Por tanto es importante dejarlo correctamente digitado y estructurado. 
                            </div>

                            <br>
                            <textarea name="new_description"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            <br>
                            
                            <h2>Comentario interno</h2>

                            <p>
                            <div class="alert alert-info">
                                <strong><?php echo $pq_3->pq_comentario_interno; ?></strong>
                            </div>
                        <?php  } ?>
                        </p>

                        <h5>Actualice el nuevo comentario interno.</h5>
                            
                            <div class="alert alert-danger">
                                <strong>Atención!</strong> Tenga en cuenta que este comentario será manejado de forma interna y no se verá publicado al público en general. Aca podrá dejar las aclaraciones que crea conveniente de manera interna. 
                            </div>

                            <br>
                            <textarea name="new_comentario"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            
                            <br>
                            
                            <h2>
                            Estado Actual
                            </h2>
                            <p>
                                <?php foreach($result_paquete3 as $old_estado){
                                    echo  '<div class="alert alert-info">
                                    <strong>'.$old_estado->e_nombre.'</strong> Este paquete está disponible para ser procesado en las matriculas y pensión de los usuarios y para ser publicada en la página de requerirse.
                                    </div>';
                                }?>
                            </p>

                            <br><br>

                            <h5>Inactivar paquete.</h5>

                            <select name="cambiar_estado" class="form-control" required>
                                <option value="su">Seleccione uno</option>

                                <?php 
                                    
                                    foreach($result_estados as $estado_new_paque){
                                    $nombre_estado=$estado_new_paque->e_nombre;
                                    $pk_nuevo_estado=$estado_new_paque->e_pk;

                                    echo '<option value="'.$pk_nuevo_estado.'">'.$nombre_estado.'</option>';
                                }
                                ?>

                            </select>
                        
                            <br>
                            <br>

                            <button type="submit" class="btn btn-primary">
                                Actualizar
                            </button>
                        </form>

                            
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->

            <!-- The Modal -->
            <div class="modal" id="paquete4">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                        <h4 class="modal-title">
                                <div class="alert alert-dark">
                                    <strong>Editar el paquete: "<?php foreach($result_paquete4 as $pq_name){
                                        echo $pq_name->pq_nombre; }?>"</strong>
                                </div>
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;
                            </button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">

                        <form action="controller/controlador.php?metodo=edit_paquete" name="matricula_form" method="POST" >

                        <input type="hidden" name="pk_paquete"  value="<?php GLOBAL $wpdb ; foreach($result_paquete4 as $pq_4){  $pk_paquete=$pq_4->pq_pk; echo $pk_paquete; } ?>">

                        <h5>ID del plan: <?php echo $pq_4->pq_id; ?></h5>

                        <h2>Cambiar el nombre del plan</h2>

                        <input type="text" name="pq_nombre" class="form-control"  placeholder="Ingrese el nuevo nombre del paquete">
                        <br><br>
                        <h2>Descripción actual del plan</h2>

                        <p>
                                
                            <div class="alert alert-info">
                            <strong><?php foreach($result_paquete4 as $pq_4){ 
                            echo $pq_4->pq_descripción; ?></strong>
                            </div>
                            
                        </p>
                        
                            <h5>Ingresar nueva descripción</h5>
                        
                            <div class="alert alert-warning">
                                <strong>Atención!</strong> Tenga en cuenta que está descripción puede publicarse en la página y podrá ser vista por los usuarios en general, Por tanto es importante dejarlo correctamente digitado y estructurado. 
                            </div>

                            <br>
                            <textarea name="new_description"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            <br>
                            
                            <h2>Comentario interno</h2>

                            <p>
                            <div class="alert alert-info">
                                <strong><?php echo $pq_4->pq_comentario_interno; ?></strong>
                            </div>
                        <?php  } ?>
                        </p>

                        <h5>Actualice el nuevo comentario interno.</h5>
                            
                            <div class="alert alert-danger">
                                <strong>Atención!</strong> Tenga en cuenta que este comentario será manejado de forma interna y no se verá publicado al público en general. Aca podrá dejar las aclaraciones que crea conveniente de manera interna. 
                            </div>

                            <br>
                            <textarea name="new_comentario"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            
                            <br>
                            
                            <h2>
                            Estado Actual
                            </h2>
                            <p>
                                <?php foreach($result_paquete4 as $old_estado){
                                    echo  '<div class="alert alert-info">
                                    <strong>'.$old_estado->e_nombre.'</strong> Este paquete está disponible para ser procesado en las matriculas y pensión de los usuarios y para ser publicada en la página de requerirse.
                                    </div>';
                                }?>
                            </p>

                            <br><br>

                            <h5>Inactivar paquete.</h5>

                            <select name="cambiar_estado" class="form-control" required>
                                <option value="su">Seleccione uno</option>

                                <?php 
                                    
                                    foreach($result_estados as $estado_new_paque){
                                    $nombre_estado=$estado_new_paque->e_nombre;
                                    $pk_nuevo_estado=$estado_new_paque->e_pk;

                                    echo '<option value="'.$pk_nuevo_estado.'">'.$nombre_estado.'</option>';
                                }
                                ?>

                            </select>
                        
                            <br>
                            <br>

                            <button type="submit" class="btn btn-primary">
                                Actualizar
                            </button>
                        </form>
                            
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->

            <!-- The Modal -->
            <div class="modal" id="paquete5">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                        <h4 class="modal-title">
                                <div class="alert alert-dark">
                                    <strong>Editar el paquete: "<?php foreach($result_paquete5 as $pq_name){
                                        echo $pq_name->pq_nombre; }?> "</strong>
                                </div>
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;
                            </button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                        <form action="controller/controlador.php?metodo=edit_paquete" name="matricula_form" method="POST" >

                        <input type="hidden" name="pk_paquete"  value="<?php GLOBAL $wpdb ; foreach($result_paquete5 as $pq_5){  $pk_paquete=$pq_5->pq_pk; echo $pk_paquete; } ?>">

                        <h5>ID del plan: <?php echo $pq_5->pq_id; ?></h5>

                        <h2>Cambiar el nombre del plan</h2>

                        <input type="text" name="pq_nombre" class="form-control"  placeholder="Ingrese el nuevo nombre del paquete">
                        <br><br>
                        <h2>Descripción actual del plan</h2>

                        <p>
                                
                            <div class="alert alert-info">
                            <strong><?php foreach($result_paquete5 as $pq_5){ 
                            echo $pq_5->pq_descripción; ?></strong>
                            </div>
                            
                        </p>
                        
                            <h5>Ingresar nueva descripción</h5>
                        
                            <div class="alert alert-warning">
                                <strong>Atención!</strong> Tenga en cuenta que está descripción puede publicarse en la página y podrá ser vista por los usuarios en general, Por tanto es importante dejarlo correctamente digitado y estructurado. 
                            </div>

                            <br>
                            <textarea name="new_description"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            <br>
                            
                            <h2>Comentario interno</h2>

                            <p>
                            <div class="alert alert-info">
                                <strong><?php echo $pq_5->pq_comentario_interno; ?></strong>
                            </div>
                        <?php  } ?>
                        </p>

                        <h5>Actualice el nuevo comentario interno.</h5>
                            
                            <div class="alert alert-danger">
                                <strong>Atención!</strong> Tenga en cuenta que este comentario será manejado de forma interna y no se verá publicado al público en general. Aca podrá dejar las aclaraciones que crea conveniente de manera interna. 
                            </div>

                            <br>
                            <textarea name="new_comentario"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            
                            <br>
                            
                            <h2>
                            Estado Actual
                            </h2>
                            <p>
                                <?php foreach($result_paquete5 as $old_estado){
                                    echo  '<div class="alert alert-info">
                                    <strong>'.$old_estado->e_nombre.'</strong> Este paquete está disponible para ser procesado en las matriculas y pensión de los usuarios y para ser publicada en la página de requerirse.
                                    </div>';
                                }?>
                            </p>

                            <br><br>

                            <h5>Inactivar paquete.</h5>

                            <select name="cambiar_estado" class="form-control" required>
                                <option value="su">Seleccione uno</option>

                                <?php 
                                    
                                    foreach($result_estados as $estado_new_paque){
                                    $nombre_estado=$estado_new_paque->e_nombre;
                                    $pk_nuevo_estado=$estado_new_paque->e_pk;

                                    echo '<option value="'.$pk_nuevo_estado.'">'.$nombre_estado.'</option>';
                                }
                                ?>

                            </select>
                        
                            <br>
                            <br>

                            <button type="submit" class="btn btn-primary">
                                Actualizar
                            </button>
                        </form>
                            
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->

            <!-- The Modal -->
            <div class="modal" id="paquete6">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                        <h4 class="modal-title">
                                <div class="alert alert-dark">
                                    <strong>Editar el paquete: "<?php foreach($result_paquete6 as $pq_name){
                                        echo $pq_name->pq_nombre; }?> "</strong>
                                </div>
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;
                            </button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">

                        <form action="controller/controlador.php?metodo=edit_paquete" name="matricula_form" method="POST" >

                        <input type="hidden" name="pk_paquete"  value="<?php GLOBAL $wpdb ; foreach($result_paquete6 as $pq_6){  $pk_paquete=$pq_6->pq_pk; echo $pk_paquete; } ?>">

                        <h5>ID del plan: <?php echo $pq_6->pq_id; ?></h5>

                        <h2>Cambiar el nombre del plan</h2>

                        <input type="text" name="pq_nombre" class="form-control"  placeholder="Ingrese el nuevo nombre del paquete">
                        <br><br>
                        <h2>Descripción actual del plan</h2>

                        <p>
                                
                            <div class="alert alert-info">
                            <strong><?php foreach($result_paquete6 as $pq_6){ 
                            echo $pq_6->pq_descripción; ?></strong>
                            </div>
                            
                        </p>
                        
                            <h5>Ingresar nueva descripción</h5>
                        
                            <div class="alert alert-warning">
                                <strong>Atención!</strong> Tenga en cuenta que está descripción puede publicarse en la página y podrá ser vista por los usuarios en general, Por tanto es importante dejarlo correctamente digitado y estructurado. 
                            </div>

                            <br>
                            <textarea name="new_description"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            <br>
                            
                            <h2>Comentario interno</h2>

                            <p>
                            <div class="alert alert-info">
                                <strong><?php echo $pq_6->pq_comentario_interno; ?></strong>
                            </div>
                        <?php  } ?>
                        </p>

                        <h5>Actualice el nuevo comentario interno.</h5>
                            
                            <div class="alert alert-danger">
                                <strong>Atención!</strong> Tenga en cuenta que este comentario será manejado de forma interna y no se verá publicado al público en general. Aca podrá dejar las aclaraciones que crea conveniente de manera interna. 
                            </div>

                            <br>
                            <textarea name="new_comentario"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            
                            <br>
                            
                            <h2>
                            Estado Actual
                            </h2>
                            <p>
                                <?php foreach($result_paquete6 as $old_estado){
                                    echo  '<div class="alert alert-info">
                                    <strong>'.$old_estado->e_nombre.'</strong> Este paquete está disponible para ser procesado en las matriculas y pensión de los usuarios y para ser publicada en la página de requerirse.
                                    </div>';
                                }?>
                            </p>

                            <br><br>

                            <h5>Inactivar paquete.</h5>

                            <select name="cambiar_estado" class="form-control" required>
                                <option value="su">Seleccione uno</option>

                                <?php 
                                    
                                    foreach($result_estados as $estado_new_paque){
                                    $nombre_estado=$estado_new_paque->e_nombre;
                                    $pk_nuevo_estado=$estado_new_paque->e_pk;

                                    echo '<option value="'.$pk_nuevo_estado.'">'.$nombre_estado.'</option>';
                                }
                                ?>

                            </select>
                        
                            <br>
                            <br>

                            <button type="submit" class="btn btn-primary">
                                Actualizar
                            </button>
                        </form>
                            
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->

            <!-- The Modal -->
            <div class="modal" id="paquete7">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                        <h4 class="modal-title">
                                <div class="alert alert-dark">
                                    <strong>Editar el paquete: "<?php foreach($result_paquete7 as $pq_name){
                                        echo $pq_name->pq_nombre; }?>"</strong>
                                </div>
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;
                            </button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">

                        <form action="controller/controlador.php?metodo=edit_paquete" name="matricula_form" method="POST" >

                        <input type="hidden" name="pk_paquete"  value="<?php GLOBAL $wpdb ; foreach($result_paquete7 as $pq_7){  $pk_paquete=$pq_7->pq_pk; echo $pk_paquete; } ?>">

                        <h5>ID del plan: <?php echo $pq_7->pq_id; ?></h5>

                        <h2>Cambiar el nombre del plan</h2>

                        <input type="text" name="pq_nombre" class="form-control"  placeholder="Ingrese el nuevo nombre del paquete">
                        <br><br>
                        <h2>Descripción actual del plan</h2>

                        <p>
                                
                            <div class="alert alert-info">
                            <strong><?php foreach($result_paquete7 as $pq_7){ 
                            echo $pq_7->pq_descripción; ?></strong>
                            </div>
                            
                        </p>
                        
                            <h5>Ingresar nueva descripción</h5>
                        
                            <div class="alert alert-warning">
                                <strong>Atención!</strong> Tenga en cuenta que está descripción puede publicarse en la página y podrá ser vista por los usuarios en general, Por tanto es importante dejarlo correctamente digitado y estructurado. 
                            </div>

                            <br>
                            <textarea name="new_description"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            <br>
                            
                            <h2>Comentario interno</h2>

                            <p>
                            <div class="alert alert-info">
                                <strong><?php echo $pq_7->pq_comentario_interno; ?></strong>
                            </div>
                        <?php  } ?>
                        </p>

                        <h5>Actualice el nuevo comentario interno.</h5>
                            
                            <div class="alert alert-danger">
                                <strong>Atención!</strong> Tenga en cuenta que este comentario será manejado de forma interna y no se verá publicado al público en general. Aca podrá dejar las aclaraciones que crea conveniente de manera interna. 
                            </div>

                            <br>
                            <textarea name="new_comentario"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            
                            <br>
                            
                            <h2>
                            Estado Actual
                            </h2>
                            <p>
                                <?php foreach($result_paquete7 as $old_estado){
                                    echo  '<div class="alert alert-info">
                                    <strong>'.$old_estado->e_nombre.'</strong> Este paquete está disponible para ser procesado en las matriculas y pensión de los usuarios y para ser publicada en la página de requerirse.
                                    </div>';
                                }?>
                            </p>

                            <br><br>

                            <h5>Inactivar paquete.</h5>

                            <select name="cambiar_estado" class="form-control" required>
                                <option value="su">Seleccione uno</option>

                                <?php 
                                    
                                    foreach($result_estados as $estado_new_paque){
                                    $nombre_estado=$estado_new_paque->e_nombre;
                                    $pk_nuevo_estado=$estado_new_paque->e_pk;

                                    echo '<option value="'.$pk_nuevo_estado.'">'.$nombre_estado.'</option>';
                                }
                                ?>

                            </select>
                        
                            <br>
                            <br>

                            <button type="submit" class="btn btn-primary">
                                Actualizar
                            </button>
                        </form>
                            
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->

            <!-- The Modal -->
            <div class="modal" id="paquete8">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                        <h4 class="modal-title">
                                <div class="alert alert-dark">
                                    <strong>Editar el paquete: "<?php foreach($result_paquete8 as $pq_name){
                                        echo $pq_name->pq_nombre; }?>"</strong>
                                </div>
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">&times;
                            </button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                        <form action="controller/controlador.php?metodo=edit_paquete" name="matricula_form" method="POST" >

                        <input type="hidden" name="pk_paquete"  value="<?php GLOBAL $wpdb ; foreach($result_paquete8 as $pq_8){  $pk_paquete=$pq_8->pq_pk; echo $pk_paquete; } ?>">

                        <h5>ID del plan: <?php echo $pq_8->pq_id; ?></h5>

                        <h2>Cambiar el nombre del plan</h2>

                        <input type="text" name="pq_nombre" class="form-control"  placeholder="Ingrese el nuevo nombre del paquete">
                        <br><br>
                        <h2>Descripción actual del plan</h2>

                        <p>
                                
                            <div class="alert alert-info">
                            <strong><?php foreach($result_paquete8 as $pq_8){ 
                            echo $pq_8->pq_descripción; ?></strong>
                            </div>
                            
                        </p>
                        
                            <h5>Ingresar nueva descripción</h5>
                        
                            <div class="alert alert-warning">
                                <strong>Atención!</strong> Tenga en cuenta que está descripción puede publicarse en la página y podrá ser vista por los usuarios en general, Por tanto es importante dejarlo correctamente digitado y estructurado. 
                            </div>

                            <br>
                            <textarea name="new_description"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            <br>
                            
                            <h2>Comentario interno</h2>

                            <p>
                            <div class="alert alert-info">
                                <strong><?php echo $pq_8->pq_comentario_interno; ?></strong>
                            </div>
                        <?php  } ?>
                        </p>

                        <h5>Actualice el nuevo comentario interno.</h5>
                            
                            <div class="alert alert-danger">
                                <strong>Atención!</strong> Tenga en cuenta que este comentario será manejado de forma interna y no se verá publicado al público en general. Aca podrá dejar las aclaraciones que crea conveniente de manera interna. 
                            </div>

                            <br>
                            <textarea name="new_comentario"  cols="90" rows="8" class="form-control"></textarea>
                            <br>
                            
                            <br>
                            
                            <h2>
                            Estado Actual
                            </h2>
                            <p>
                                <?php foreach($result_paquete8 as $old_estado){
                                    echo  '<div class="alert alert-info">
                                    <strong>'.$old_estado->e_nombre.'</strong> Este paquete está disponible para ser procesado en las matriculas y pensión de los usuarios y para ser publicada en la página de requerirse.
                                    </div>';
                                }?>
                            </p>

                            <br><br>

                            <h5>Inactivar paquete.</h5>

                            <select name="cambiar_estado" class="form-control" required>
                                <option value="su">Seleccione uno</option>

                                <?php 
                                    
                                    foreach($result_estados as $estado_new_paque){
                                    $nombre_estado=$estado_new_paque->e_nombre;
                                    $pk_nuevo_estado=$estado_new_paque->e_pk;

                                    echo '<option value="'.$pk_nuevo_estado.'">'.$nombre_estado.'</option>';
                                }
                                ?>

                            </select>
                        
                            <br>
                            <br>

                            <button type="submit" class="btn btn-primary">
                                Actualizar
                            </button>
                        </form>
                            
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->


            <br><br>

<center> 
    <div class="container">
        <div class="container-fluid">
            <div class="d-inline-flex p-3 bg-secondary text-white">  
                <div class="p-2 bg-info">
                    <h1>
                    <svg class="bi bi-bag-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 4h14v10a2 2 0 01-2 2H3a2 2 0 01-2-2V4zm7-2.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
                    </svg>
                    PROGRAMAS</h1>
                    <p>Aca se listan los totales de enrolamientos activos a la fecha en los diferentes programas. Podrá acceder al Listado de los usuarios agrupados por el detalle de formación.</p>
                </div>
            </div>
        </div>
    </div>
</center>
<br>



<div class="container-fluid">
    <div class="row">
        <div class="col" >
            <h3>Kickboxing 
             <!-- Button to Open the Modal -->
                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#kb">
                Ver
                    <svg class="bi bi-eye-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.5 8a2.5 2.5 0 11-5 0 2.5 2.5 0 015 0z"/>
                        <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 100-7 3.5 3.5 0 000 7z" clip-rule="evenodd"/>
                    </svg>
                </button>
            </h3> 

            <!-- The Modal -->
            <div class="modal" id="kb">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Listado de enrolamientos <strong style="color:red;">Kickboxing</strong></h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <table class="table table-dark table-hover">
                                <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Tipo de formación</th>
                                    <th>Correo</th>
                                    <th>Número de contacto</th>
                                    <th>Fecha de enrolamiento</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php


                                foreach($result_all_programasKB as $program){
                                    $name=$program->u_nombre;
                                    $lastName=$program->u_apellido;
                                    $type_prog=$program->p_nombre;
                                    $email=$program->u_correo_e;
                                    $tel=$program->u_celular;
                                    $enrollDate=$program->er_fecha_creacion;
                                    
                                

                                ?>
                                <tr>
                                    <td><?php echo $name;?></td>
                                    <td><?php echo $lastName;?></td>
                                    <td><?php echo $type_prog;?></td>
                                    <td><?php echo $email;?></td>
                                    <td><?php echo $tel;?></td>
                                <td><?php echo  $enrollDate;?></td>
                                </tr>
                                </tbody>
                                <?php } ?>
                            </table>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->
            <table class="table table-hover table-bordered ">
                <thead>
                    <tr>
                        <th>Ejecutivo</th>
                        <th>Deportivo</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 

                        GLOBAL $wpdb;

                        $result_kb_ejecutivo=0;
                        $result_kb_deportivo=0;
                        $result_tk_inf=0;
                        $result_tk_deportivo=0;
                        $result_exploracion=0;
                        $result_music_ini=0;
                        $result_music_musica=0;

                            foreach($result_todos_programas as $program){
                                

                                if($program->p_id==2){

                                    $result_kb_ejecutivo++;
                                }

                                if($program->p_id==1){

                                    $result_kb_deportivo++;
                                   
                                }
                                
                                if($program->p_id==4){

                                    $result_tk_inf++;
                                    
                                }

                                if($program->p_id==3){

                                    $result_tk_deportivo++;
                                    
                                }
                                

                                if($program->p_id==5){

                                    $result_exploracion++;
                                    
                                }

                                if($program->p_id==7){

                                    $result_music_ini++;

                                }

                                if($program->p_id==8){

                                    $result_music_musica++;
                                    
                                }

                            }

                    ?>

                    <tr style="text-align:center; color:#244C67;">
                        <td style="color:blue;"><?php echo $result_kb_ejecutivo; ?>
                         </td>
                        <td style="color:blue;"><?php echo $result_kb_deportivo;?></td>
                        <td style="color:red;"><strong><?php  $total_kb=$result_kb_ejecutivo+$result_kb_deportivo; 
                        echo $total_kb;   ?></strong></td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div class="col" >
            <h3>Taekwondo
                 <!-- Button to Open the Modal -->
                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#tk">
                    Ver
                    <svg class="bi bi-eye-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.5 8a2.5 2.5 0 11-5 0 2.5 2.5 0 015 0z"/>
                        <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 100-7 3.5 3.5 0 000 7z" clip-rule="evenodd"/>
                    </svg>
                </button>
            </h3> 

              <!-- The Modal -->
            <div class="modal" id="tk">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Listado de enrolamientos <strong style="color:red;">Taekwondo</strong></h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                        <table class="table table-dark table-hover">
                                <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Tipo de formación</th>
                                    <th>Correo</th>
                                    <th>Número de contacto</th>
                                    <th>Fecha de enrolamiento</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($result_all_programastk as $program){
                                    $name=$program->u_nombre;
                                    $lastName=$program->u_apellido;
                                    $type_prog=$program->p_nombre;
                                    $email=$program->u_correo_e;
                                    $tel=$program->u_celular;
                                    $enrollDate=$program->er_fecha_creacion;
                                    
                                

                                ?>
                                <tr>
                                    <td><?php echo $name;?></td>
                                    <td><?php echo $lastName;?></td>
                                    <td><?php echo $type_prog;?></td>
                                    <td><?php echo $email;?></td>
                                    <td><?php echo $tel;?></td>
                                    <td><?php echo $enrollDate;?></td>
                                </tr>
                                </tbody>
                                <?php } ?>
                            </table>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->

            <table class="table table-hover table-bordered ">
                <thead>
                    <tr>
                        <th>Infantil</th>
                        <th>Deportivo</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="text-align:center; color:#244C67;">
                        <td style="color:blue;"><?php echo $result_tk_inf; ?></td>
                        <td style="color:blue;"><?php echo $result_tk_deportivo; ?></td>
                        <td style="color:red;"><strong><?php $total_taekwondo=$result_tk_deportivo+$result_tk_inf; echo $total_taekwondo; ?></strong></td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div class="col" > 
            <h3>Exploración
                 <!-- Button to Open the Modal -->
                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#ex">
                    Ver
                    <svg class="bi bi-eye-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.5 8a2.5 2.5 0 11-5 0 2.5 2.5 0 015 0z"/>
                        <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 100-7 3.5 3.5 0 000 7z" clip-rule="evenodd"/>
                    </svg>
                </button>
            </h3> 

              <!-- The Modal -->
            <div class="modal" id="ex">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Listado de enrolamientos <strong style="color:red;">Exploración</strong></h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                        <table class="table table-dark table-hover">
                                <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Tipo de formación</th>
                                    <th>Correo</th>
                                    <th>Número de contacto</th>
                                    <th>Fecha de enrolamiento</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($result_all_programasex as $program){
                                    $name=$program->u_nombre;
                                    $lastName=$program->u_apellido;
                                    $type_prog=$program->p_nombre;
                                    $email=$program->u_correo_e;
                                    $tel=$program->u_celular;
                                    $enrollDate=$program->er_fecha_creacion;
                                    
                                

                                ?>
                                <tr>
                                    <td><?php echo $name;?></td>
                                    <td><?php echo $lastName;?></td>
                                    <td><?php echo $type_prog;?></td>
                                    <td><?php echo $email;?></td>
                                    <td><?php echo $tel;?></td>
                                    <td><?php echo $enrollDate;?></td>
                                </tr>
                                </tbody>
                                <?php } ?>
                            </table>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->

            <table class="table table-hover table-bordered ">
                <thead>
                    <tr>
                        <th>Total niños</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="text-align:center; color:#244C67;">
                        <td style="color:red;"><strong><?php echo $result_exploracion; ?></strong></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col" > 
            <h3>Música
                 <!-- Button to Open the Modal -->
                 <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#ms">
                    Ver
                    <svg class="bi bi-eye-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.5 8a2.5 2.5 0 11-5 0 2.5 2.5 0 015 0z"/>
                        <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 100-7 3.5 3.5 0 000 7z" clip-rule="evenodd"/>
                    </svg>
                </button>
            </h3> 

              <!-- The Modal -->
            <div class="modal" id="ms">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Listado de enrolamientos <strong style="color:red;">Música</strong></h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                            <table class="table table-dark table-hover">
                                <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Tipo de formación</th>
                                    <th>Correo</th>
                                    <th>Número de contacto</th>
                                    <th>Fecha de enrolamiento</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach($result_all_programasmusic as $program){
                                    $name=$program->u_nombre;
                                    $lastName=$program->u_apellido;
                                    $type_prog=$program->p_nombre;
                                    $email=$program->u_correo_e;
                                    $tel=$program->u_celular;
                                    $enrollDate=$program->er_fecha_creacion;
                                    
                                

                                ?>
                                <tr>
                                    <td><?php echo $name;?></td>
                                    <td><?php echo $lastName;?></td>
                                    <td><?php echo $type_prog;?></td>
                                    <td><?php echo $email;?></td>
                                    <td><?php echo $tel;?></td>
                                    <td><?php echo $enrollDate;?></td>
                                </tr>
                                </tbody>
                                <?php } ?>
                            </table>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar Ventana</button>
                        </div>
                    </div>
                </div>
            </div> <!-- acá cierra The Modal -->

            <table class="table table-hover table-bordered ">
                <thead>
                    <tr>
                        <th>Iniciación</th>
                        <th>Música</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="text-align:center; color:#244C67;">
                        <td style="color:blue;"><?php echo $result_music_ini; ?></td>
                        <td style="color:blue;"><?php echo $result_music_musica;?></td>
                        <td style="color:red;"><strong><?php echo $total_musica=$result_music_musica+$result_music_ini; ?></strong></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col" > 
            <h3 style="text-align:center;">
            <svg class="bi bi-plus" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M8 3.5a.5.5 0 01.5.5v4a.5.5 0 01-.5.5H4a.5.5 0 010-1h3.5V4a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
                <path fill-rule="evenodd" d="M7.5 8a.5.5 0 01.5-.5h4a.5.5 0 010 1H8.5V12a.5.5 0 01-1 0V8z" clip-rule="evenodd"/>
            </svg>
            Total de enrolamientos activos</h3>            
            <table class="table table-hover table-bordered ">
                <tbody>
                    <tr style="text-align:center;">
                        <td><strong><p style="color:red; font-size:25px"><?php echo $total_kb+$total_taekwondo+$result_exploracion+$total_musica; ?></p></strong></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<br><br>

<center> 
    <div class="container">
        <div class="container-fluid">
            <div class="d-inline-flex p-3 bg-secondary text-white">  
                <div class="p-2 bg-info">
                    <h1> 
                    <svg class="bi bi-emoji-dizzy" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path fill-rule="evenodd" d="M9.146 5.146a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708.708l-.647.646.647.646a.5.5 0 0 1-.708.708l-.646-.647-.646.647a.5.5 0 1 1-.708-.708l.647-.646-.647-.646a.5.5 0 0 1 0-.708zm-5 0a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 1 1 .708.708l-.647.646.647.646a.5.5 0 1 1-.708.708L5.5 7.207l-.646.647a.5.5 0 1 1-.708-.708l.647-.646-.647-.646a.5.5 0 0 1 0-.708z"/>
                        <path d="M10 11a2 2 0 1 1-4 0 2 2 0 0 1 4 0z"/>
                    </svg>
                    Mensualidades vencidas</h1>
                    <p>Se listaran los usuarios que ya tienen su mensualidad vencida a la fecha.</p>
                </div>
            </div>
        </div>
    </div>
</center>
<br>


<div class="container">

  <h3>Usuarios en mora a la fecha (<?php echo date("d - M - Y"); ?> )</h3>            
  <table class="table table-hover table-bordered table-sm table-striped">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Fecha de vencimiento</th>
        <th>Enviar recordatorio</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>12</td>
        <td><button type="button" class="btn btn-warning btn-sm">
            <svg class="bi bi-envelope-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M.05 3.555L8 8.414l7.95-4.859A2 2 0 0014 2H2A2 2 0 00.05 3.555zM16 4.697l-5.875 3.59L16 11.743V4.697zm-.168 8.108L9.157 8.879 8 9.586l-1.157-.707-6.675 3.926A2 2 0 002 14h12a2 2 0 001.832-1.195zM0 11.743l5.875-3.456L0 4.697v7.046z"/>
            </svg>
        
        </button></td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>12</td>
        <td>25 años</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>12</td>
        <td>25 años</td>
      </tr>
    </tbody>
  </table>
</div>
<br><br>

<center> 
    <div class="container">
        <div class="container-fluid">
            <div class="d-inline-flex p-3 bg-secondary text-white">  
                <div class="p-2 bg-info">
                    <h1>
                    
                    <svg class="bi bi-gift-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M10 1a1.5 1.5 0 0 0-1.5 1.5c0 .098.033.16.12.227.103.081.272.15.49.2A3.44 3.44 0 0 0 9.96 3h.015L10 2.999l.025.002h.014A2.569 2.569 0 0 0 10.293 3c.17-.006.387-.026.598-.073.217-.048.386-.118.49-.199.086-.066.119-.13.119-.227A1.5 1.5 0 0 0 10 1zm0 3h-.006a3.535 3.535 0 0 1-.326 0 4.435 4.435 0 0 1-.777-.097c-.283-.063-.614-.175-.885-.385A1.255 1.255 0 0 1 7.5 2.5a2.5 2.5 0 0 1 5 0c0 .454-.217.793-.506 1.017-.27.21-.602.322-.885.385a4.434 4.434 0 0 1-1.104.099H10z"/>
                        <path fill-rule="evenodd" d="M6 1a1.5 1.5 0 0 0-1.5 1.5c0 .098.033.16.12.227.103.081.272.15.49.2A3.44 3.44 0 0 0 5.96 3h.015L6 2.999l.025.002h.014l.053.001a3.869 3.869 0 0 0 .799-.076c.217-.048.386-.118.49-.199.086-.066.119-.13.119-.227A1.5 1.5 0 0 0 6 1zm0 3h-.006a3.535 3.535 0 0 1-.326 0 4.435 4.435 0 0 1-.777-.097c-.283-.063-.614-.175-.885-.385A1.255 1.255 0 0 1 3.5 2.5a2.5 2.5 0 0 1 5 0c0 .454-.217.793-.506 1.017-.27.21-.602.322-.885.385a4.435 4.435 0 0 1-1.103.099H6zm9 10.5V7H8.5v9h5a1.5 1.5 0 0 0 1.5-1.5zM7.5 16h-5A1.5 1.5 0 0 1 1 14.5V7h6.5v9z"/>
                        <path d="M0 4a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V4z"/>
                    </svg>

                    <svg class="bi bi-emoji-sunglasses" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        <path fill-rule="evenodd" d="M4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM6.5 6.497V6.5h-1c0-.568.447-.947.862-1.154C6.807 5.123 7.387 5 8 5s1.193.123 1.638.346c.415.207.862.586.862 1.154h-1v-.003l-.003-.01a.213.213 0 0 0-.036-.053.86.86 0 0 0-.27-.194C8.91 6.1 8.49 6 8 6c-.491 0-.912.1-1.19.24a.86.86 0 0 0-.271.194.213.213 0 0 0-.036.054l-.003.01z"/>
                        <path d="M2.31 5.243A1 1 0 0 1 3.28 4H6a1 1 0 0 1 1 1v1a2 2 0 0 1-2 2h-.438a2 2 0 0 1-1.94-1.515L2.31 5.243zM9 5a1 1 0 0 1 1-1h2.72a1 1 0 0 1 .97 1.243l-.311 1.242A2 2 0 0 1 11.439 8H11a2 2 0 0 1-2-2V5z"/>
                    </svg>
                                                                CUMPLEAÑOS</h1>
                    <p>Acá se mostrará los usuarios que cumplen años en el mes actual.</p>
                </div>
            </div>
        </div>
    </div>
</center>

<br>

<div class="container">
  <h3>
  Cumpleaños del mes de <?php echo date("M - Y"); ?></h3>            
  <table class="table table-hover table-bordered table-sm table-striped">
    <thead style="text-align:center;">
      <tr>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Documento</th>
        <th>Correo</th>
        <th>Día de cumpleaños</th>
        <th>Cumplió</th>
      </tr>
    </thead>
    <tbody>
    <?php 
    foreach($result_detalle_usuario as $user_cumple){

        $name=$user_cumple->u_nombre;
        $apellido=$user_cumple->u_apellido;
        $birth_date=$user_cumple->u_fecha_nacimiento;
        $doc=$user_cumple->u_documento;
        $mail=$user_cumple->u_correo_e;
        $year_actual=date('Y');
        $yearB=[];
        $yearB = explode ('-',$birth_date);
        $edad= $year_actual-$yearB[0];
        $mes_actual=date('m');
        $mes_del_registro=strtotime($birth_date);
        $mes_cumple=date("m",$mes_del_registro);
        

        if($mes_actual===$mes_cumple){

            echo '
            <tr>
              <td style="">'.$name.'</td>
              <td style="">'.$apellido.'</td>
              <td style="text-align:center;">'.$doc.'</td>
              <td style="">'.$mail.'</td>
              <td style="text-align:center;">'.$birth_date.'</td>
              <td style="text-align:center;">'.$edad.'</td>
            </tr>';

        }

       
    } 
    ?>
    </tbody>
  </table>
</div>






