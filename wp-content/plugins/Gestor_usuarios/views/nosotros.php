<div class="jumbotron">
    <h1>Quienes somos</h1>      
    <p>Promovemos, desarrollamos y masificamos programas deportivos especializados para cada tipo de población, buscando la inclusión y el aprovechamiento del tiempo libre para mejorar la calidad de vida de los colombianos.</p>
  </div>

<div class="container-fluid">
    <div class="row">
        <div class="col" style="background-color:white;">
            <center><img src="http://localhost/expofighting/wp-content/uploads/2020/02/logo-completo.png" alt=""></center>
        </div>
        <div class="col" style="background-color:white;">
            <P>Expofighting es un centro deportivo colombiano que nace en el año 2011 en la ciudad de Bogotá dedicada a la preparación de deportistas de alto rendimiento en las artes marciales de Taekwondo WTF y Kick Boxing, como también programas para la población que requiere acondicionamiento físico de una manera divertida.</P>

            <P>Desde el año de 1986 estamos formando artistas marciales y desarrollando programas deportivos para niños desde los 2 1/2 años, jóvenes y adultos, promoviendo principios y valores que aportan a la mejora de la sociedad. </P>

            <P>Así mismo ofrecemos un programa complementario en apreciación musical que les brinda a todos nuestros practicantes opciones para desarrollar la concentración en un ambiente de esparcimiento para  la recreación de forma saludable no solo para el cuerpo, sino también para el pensamiento.</P>
        </div>
    </div>
</div>

<div class="alert alert-warning">
    <strong>Te invitamos a </strong> Retar tu cuerpo, a cambia tu actitud y conquista tu mente
</div>


<h1>NUESTRA SEDE</h1>

<P>Contamos con una sede de dos plantas, totalmente dotadas para brindar a los afiliados un ambiente, seguro y adecuado para la práctica deportiva.  Cuenta con las siguientes características:</P>

<ul>
    <li>Área de 350 metros cuadrados.</li>
    <li>Equipamiento para  acondicionamiento físico y entrenamiento funcional.</li>
    <li>Sala de espera.</li>
    <li>Vestieres y Baños con ducha.</li>
    <li>Venta  de bebidas y snacks.</li>
    <li>Venta  de implementos.</li>
    <li>TV. </li>
    <li>Zona Wi-fi.</li>
    <li>Cámaras de seguridad en toda nuestra instalación.</li>
    <li>CONTACTO</li>
</ul>

<?php
echo do_shortcode('[smartslider3 slider=7]');
?>










