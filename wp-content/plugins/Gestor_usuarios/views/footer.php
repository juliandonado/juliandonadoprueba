<footer class="wrapper site-footer" role="contentinfo">
		<div class="container">
    <!----------- Footer ------------>
    <footer class="footer-bs">
        <div class="row">
        	<div class="col-md-2 footer-brand animated fadeInLeft">
            	<img src="http://localhost/expofighting/wp-content/uploads/2020/02/logo-Blanco-1.png" alt="">
				<br>
                <p>Contribuimos al mejoramiento de tu calidad de vida con espacios para el cuidado
y desarrollo del cuerpo humano.</p>
<p>Calle 8 # 77 - 32 Castilla Bogotá</p>
            </div>
        	<div class="col-md-8 footer-nav animated fadeInUp">
            	<h4>Ubicanos —</h4>
            	<div class="col-md-12">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3976.751822322127!2d-74.14370358568638!3d4.638304643488029!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f9c10aa9f0cbf%3A0xd5d42190c1ba6de9!2sCentro%20Deportivo%20Expofighting!5e0!3m2!1ses!2sco!4v1582321796068!5m2!1ses!2sco" width="800" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            	
            </div>
        	<div class="col-md-2 footer-social animated fadeInDown">
            	<h4>Síguenos</h4>
            	<ul>
                	<li><a href="https://www.facebook.com/expofighting/">Facebook</a></li>
                	<li><a href="https://www.instagram.com/expofighting/?hl=es">Instagram</a></li>
                	<li><a href="https://www.youtube.com/channel/UCMfEvBA3Sl7MDDyGpjKu_Jg?disable_polymer=true&nv=1">YouTube</a></li>
                </ul>
            </div>
        	
        </div>
    </footer>
    <section style="text-align:center; margin:10px auto;"><p>Sitio desarollado por <a href="##">	Julián Donado B.</a></p></section>

</div>

<style>
.footer-bs {
    background-color: #3c3d41;
	padding: 60px 40px;
	color: rgba(255,255,255,1.00);
	margin-bottom: 20px;
	border-bottom-right-radius: 6px;
	border-top-left-radius: 0px;
	border-bottom-left-radius: 6px;
}
.footer-bs .footer-brand, .footer-bs .footer-nav, .footer-bs .footer-social, .footer-bs .footer-ns { padding:10px 25px; }
.footer-bs .footer-nav, .footer-bs .footer-social, .footer-bs .footer-ns { border-color: transparent; }
.footer-bs .footer-brand h2 { margin:0px 0px 10px; }
.footer-bs .footer-brand p { font-size:12px; color:rgba(255,255,255,0.70); }

.footer-bs .footer-nav ul.pages { list-style:none; padding:0px; }
.footer-bs .footer-nav ul.pages li { padding:5px 0px;}
.footer-bs .footer-nav ul.pages a { color:rgba(255,255,255,1.00); font-weight:bold; text-transform:uppercase; }
.footer-bs .footer-nav ul.pages a:hover { color:rgba(255,255,255,0.80); text-decoration:none; }
.footer-bs .footer-nav h4 {
	font-size: 11px;
	text-transform: uppercase;
	letter-spacing: 3px;
	margin-bottom:10px;
	color: #e9563d;
}

.footer-bs .footer-nav ul.list { list-style:none; padding:0px; }
.footer-bs .footer-nav ul.list li { padding:5px 0px;}
.footer-bs .footer-nav ul.list a { color:rgba(255,255,255,0.80); }
.footer-bs .footer-nav ul.list a:hover { color:rgba(255,255,255,0.60); text-decoration:none; }

.footer-bs .footer-social ul { list-style:none; padding:0px; }
.footer-bs .footer-social h4 {
	font-size: 11px;
	text-transform: uppercase;
	letter-spacing: 3px;
	color: #e9563d;
}
.footer-bs .footer-social li { padding:5px 4px;}
.footer-bs .footer-social a { color:rgba(255,255,255,1.00);}
.footer-bs .footer-social a:hover { color:rgba(255,255,255,0.80); text-decoration:none; }

.footer-bs .footer-ns h4 {
	font-size: 11px;
	text-transform: uppercase;
	letter-spacing: 3px;
	margin-bottom:10px;
	
}
.footer-bs .footer-ns p { font-size:12px; color:rgba(255,255,255,0.70); }

@media (min-width: 768px) {
	.footer-bs .footer-nav, .footer-bs .footer-social, .footer-bs .footer-ns { border-left:solid 1px rgba(255,255,255,0.10); }
}
</style>