
<div class="jumbotron text-center">
  <h1>TAEKWONDO</h1>
  <p>La expresión "taekwondo" es formada por tres términos: tae (patada), kwon (puño), do (camino o filosofía) </p> 
</div>

<h1>Galería de imágenes</h1>

<?php
echo do_shortcode('[smartslider3 slider=6]');
?>



<h1>Taekwondo Infantil</h1>

<p> En una atmósfera agradable y familiar, los niños comienzan en el maravilloso mundo del Taekwondo de alto nivel, aprenderán las técnicas de este arte marcial, mientras comparten y cooperan con los otros practicantes, El Taekwondo desarrolla la confianza en sí mismos, la fuerza física, la agilidad, el enfoque, plantea metas a corto, mediano y largo plazo, que son comprendidas por el niño, entre otras ventajas.</p>

<h1>Horarios</h1>

<h4>Taekwondo Infantil</h4>

<div class="container">
<table class="table table-hover">
        <thead>
        <tr>
            <th>LUNES</th>
            <th>MARTES</th>
            <th>MIÉRCOLES</th>
            <th>JUEVES</th>
            <th>VIERNES</th>
            <th>SABADO</th>
            <th>DOMINGO</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>6:00 pm</td>
            <td></td>
            <td>6:00 pm</td>
            <td></td>
            <td>6:00 pm</td>
            <td>10:00 am</td>
            <td>9:00 am</td>
        </tr>
        </tbody>
    </table>
</div>


<h1>Taekwondo Deportivo</h1>
<p>Buscamos que el alumno del programa Inga Taekwondo aprenda a usar efectivamente su cuerpo como un arma de defensa personal, al mismo tiempo que refuerza sus valores y autoestima, aprendiendo un arte marcial moderno y divertido. En esta práctica, los jóvenes mejoran su disciplina, desarrollando una excelente condición física, adquieren velocidad y precisión en los golpes que se implementan en el arte marcial y mejoran la elasticidad muscular. Afianzan la confianza en sí mismos.
El Taekwondo como deporte olímpico hace que jóvenes y adultos se interesen en la competencia, pueden aprender las técnicas y estrategias de esta modalidad, para lograr así participar exitosamente en las competencias programadas por la Liga de Taekwondo de Fuerzas Armadas y la Federación Colombiana de Taekwondo, como miembros del club INGA.</p>

<h1>Horarios</h1>

<h4>Taekwondo Deportivo</h4>

<div class="container">
<table class="table table-hover">
        <thead>
        <tr>
            <th>LUNES</th>
            <th>MARTES</th>
            <th>MIÉRCOLES</th>
            <th>JUEVES</th>
            <th>VIERNES</th>
            <th>SABADO</th>
            <th>DOMINGO</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>7:00 pm</td>
            <td></td>
            <td>7:00 pm</td>
            <td></td>
            <td>7:00 pm</td>
            <td>10:00 am</td>
            <td>9:00 am</td>
        </tr>
        </tbody>
    </table>
</div>

