<div class="jumbotron text-center">
  <h1>Agenda tu cita con nuestra fisioterapeuta</h1>
  <p>En Expofighting interesados por el bienestar de nuestros afiliados hemos realizado una alianza con una fisioterapeuta profesional contamos con una fisioterapeuta <strong>(independiente)</strong>. Ella te podrá atender en ciertos horarios y días específicos para revisar tus lesiones, con esto lo que queremos lograr es que te permita entrenar de una forma segura y sana para tu cuerpo y que no afecten o agraven aún mas tus lesiones.</p> 
</div>


<h1>Nuestra fisioterapeuta Sandra Mazuera</h1>

<center><img src="http://localhost/expofighting/wp-content/uploads/2020/04/foto_Sandra_fisio-1.jpeg" class="ml-3 mt-3 rounded-circle" style="width:60%;"></center>


<p>Profesional en fisioterapia de la Universidad Manuela Beltran con 4 años de experiencia en el área osteomuscular/musculoesqueletica, ademas de contar con certificaciones internacionales y experticia en tecnicas como cinta kinesiologica: kinesiotape, dynamic tape y ventosas. </p>

<p>Actualmente practicante de taekwondo deportivo cinturon rojo en centro deportivo expofighting.</p>

<h1>Que proceso se lleva en fisioterapia?</h1>

<h3>Priemra cita</h3>

  <p>Inicialmente se debe agendar la cita para la valoración inicial, donde se emitirá un concepto donde se realizarán algunas indicaciones o contradicciones para el tipo de programa al que te inscribiste.</p>

<h3>Tratamiento</h3>
  <p>Posterior a la cita inicial donde se determinará la cantidad de terapias que el usuario requiere y cuidados que debe tener, se realizará un agendamiento de acuerdo a la disposición de la fisioterapeuta donde se realizan diferentes tipos de ejercicios buscando fortalecer los músculos lesionado permitiendo un mejoramiento en la movilidad del mismo. Estas citas serán agendadas por la fisioterapeuta.</p>

  <div class="container">
  <h2>En que horarios se realizan consultas?</h2>
  <p>Se dispondrá de una franja de una hora en la cual se podrá atender a un usuario en un promedio de 20 minutos (3 usuarios máximo en dicha hora).</p>
 
<!-- Button trigger modal -->
<a class="btn btn-primary btn-lg" href="#myModal1" data-toggle="modal">Launch demo modal</a>

<!-- Modal -->
<div id="myModal1" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">×</button>
                    <h4 class="modal-title">My Title in a Modal Window</h4>
            </div>
            <div class="modal-body">This is the body of a modal...</div>
            <div class="modal-footer">This is the footer of a modal...</div>
            </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="container">
  <h2>Modal Example</h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Lunes</th>
        <th>Martes</th>
        <th>Miercoles</th>
        <th>Jueves</th>
        <th>viernes</th>
        <th>Sabado</th>
        <th>Domingo</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td><td>
        <td>5:00 a 6:00 pm</td>
        <td></td>
        <td>7:00 a 8:00 am</td>
      </tr>
    </tbody>
  </table>
</div>
 
<h2>INICIA TU PROCESO</h2>

<div class="container">

    <div class="alert alert-info">
        <strong>Información!</strong> Debes ingresar tu número de documento, si tu cédula aparece activa en nuetra base de datos, podrás agendar la cita de fisitría.</a>.
    </div>
    
    <form class="needs-validation" action="./controller/controlador.php?metodo=consultar_miusuario" name="consultar_mi_usuario" method="POST" action_page.php">
        <label>Ingresa tu número de documento:</label> 
        <input type="number" class="form-control" id="uname" placeholder="Sólo ingrresar números" name="usuario" required>
        <button type="submit" class="btn btn-primary">Consultar</button>
    </form>
</div>