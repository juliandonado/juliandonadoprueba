<!---Estilos par el on fo--->

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 50px;
  height: 24px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>


<div class="jumbotron text-center">
    <h1>Administración de fisioterapias</h1>
    <p>Por medio de este módulo podrá generar los espacios para las citas de fisioterapía y agendar las citas de tratamiento y agregar anotaciones al usuario.</p> 
</div>

<h1>Crear las opciones de cita para valoración:</h1>
<div class="container">
    <h3>Opción 1</h3>



    <div class="row">
            <div class="col">
                <input type="datetime-local" value="2020-03-13T13:00" name="fechahora">
            </div>
            <div class="col">
                <p>Dejar como disponible?</p>
                <label class="switch">
                    <input type="checkbox" checked name="estado">
                <span class="slider round"></span>
                </label>
            </div>
            <button type="button" class="btn btn-primary" onclick="actualizarCita()">ACTUALIZAR</button>
        </div>

        <div class="row">
        </br></br>
        </div>

        <script>
        function actualizarCita(){
            alert('Se activó la función!!!')
        }
        </script>





    <form class="needs-validation" action="./controller/controlador.php?metodo=crear_agendar" name="agenda_fisio" method="POST" action_page.php">
        <div class="row">
            <div class="col">
                <input type="datetime-local" value="2020-03-13T13:00" name="fechahora">
            </div>
            <div class="col">
                <p>Dejar como disponible?</p>
                <label class="switch">
                    <input type="checkbox" checked name="activar">
                <span class="slider round"></span>
                </label>
            </div>
            <button type="submit" class="btn btn-primary">ACTUALIZAR</button>
        </div>
        </form>
    <h3>Opción 2</h3>
    <form class="needs-validation" action="./controller/controlador.php?metodo=agendar_fisio1" name="agenda_fisio" method="POST" action_page.php">
    <div class="row">
        <div class="col">
            <input type="datetime-local" value="2020-03-13T13:00" name="fechahora">
        </div>
        <div class="col">
            <p>Dejar como disponible?</p>
            <label class="switch">
                <input type="checkbox" checked name="activar">
            <span class="slider round"></span>
            </label>
        </div>
        <button type="submit" class="btn btn-primary">ACTUALIZAR</button>
    </div>
    </form>
    <h3>Opción 3</h3>
    <form class="needs-validation" action="./controller/controlador.php?metodo=agendar_fisio2" name="agenda_fisio" method="POST" action_page.php">
    <div class="row">
        <div class="col">
            <input type="datetime-local" value="2020-03-13T13:00" name="fechahora">
        </div>
        <div class="col">
            <p>Dejar como disponible?</p>
            <label class="switch">
                <input type="checkbox" checked name="activar">
            <span class="slider round"></span>
            </label>
        </div>
        <button type="submit" class="btn btn-primary">ACTUALIZAR</button>
        </div>
    </form>
    <h3>Opción 4</h3>
    <form class="needs-validation" action="./controller/controlador.php?metodo=agendar_fisio3" name="agenda_fisio" method="POST" action_page.php">
    <div class="row">
        <div class="col">
            <input type="datetime-local" value="2020-03-13T13:00" name="fechahora">
        </div>
        <div class="col">
            <p>Dejar como disponible?</p>
            <label class="switch">
                <input type="checkbox" checked name="activar">
            <span class="slider round"></span>
            </label>
        </div>
        <button type="submit" class="btn btn-primary">ACTUALIZAR</button>
    </div>
    </form>
    <h3>Opción 5</h3>
    <form class="needs-validation" action="./controller/controlador.php?metodo=agendar_fisio4" name="agenda_fisio" method="POST" action_page.php">
    <div class="row">
        <div class="col">
            <input type="datetime-local" value="2020-03-13T13:00" name="fechahora">
        </div>
        <div class="col">
            <p>Dejar como disponible?</p>
            <label class="switch">
                <input type="checkbox" checked name="activar">
            <span class="slider round"></span>
            </label>
        </div>
        <button type="submit" class="btn btn-primary">ACTUALIZAR</button>
    </div>
    </form>
    <h3>Opción 6</h3>
    <form class="needs-validation" action="./controller/controlador.php?metodo=agendar_fisio5" name="agenda_fisio" method="POST" action_page.php">
    <div class="row">
        <div class="col">
            <input type="datetime-local" value="2020-03-13T13:00" name="fechahora">
        </div>
        <div class="col">
            <p>Dejar como disponible?</p>
            <label class="switch">
                <input type="checkbox" checked name="activar">
            <span class="slider round"></span>
            </label>
        </div>
        <button type="submit" class="btn btn-primary">ACTUALIZAR</button>
    </div>
    </form>
</div>

 






