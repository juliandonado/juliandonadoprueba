
<div class="jumbotron text-center">
  <h1>Solicitud de cita para fisiatria</h1>
  <p>Ya que se validó con éxito que estás activo en nuestra base de datos puedes acceder a la cita de fisiatria.</p> 
</div>

<h1>Programar Cita</h1>

<p>Selecciona la fecha y la hora en que deseas agendar tu cita</p>

<form class="needs-validation" action="./controller/controlador.php?metodo=agendar_fisio" name="agenda_fisio" method="POST" action_page.php">
    <div class="row">
      <div class="col">
        <input type="datetime-local" value="2020-03-13T13:00" name="fechahora">
        <?php //foreach($tipoCita as $consulta){
          //]?>
        <input name="tipo_cita" type="hidden" value="">
        <input name="prodId" type="hidden" value="xm234jq">
      </div>
      <button type="submit" class="btn btn-primary">Agendar</button>
    </div>
  </form>
