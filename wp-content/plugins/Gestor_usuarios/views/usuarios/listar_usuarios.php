<script>
jQuery(document).ready(function(){
    jQuery("#myInput").on("keyup", function() {
    var value = jQuery(this).val().toLowerCase();
    jQuery("#myTable tr").filter(function() {
        jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>


<center> 
    <div class="container">
        <div class="container-fluid">
            <div class="d-inline-flex p-3 bg-secondary text-white">  
                <div class="p-2 bg-info">
                    <h1>
                    <svg class="bi bi-people-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
                    </svg>
                    Ver usuarios creados</h1>
                    <p>Acá se listarán todos los usuarios creados con información básica personal y su estado actual.</p>
                </div>
            </div>
        </div>
    </div>
</center> 

<div class="container mt-3">
  <p>Si desaa buscar algún usuario en particular puede buscar por cualqueir dato visualizado en la siguiente tabla</p>  
  <input class="form-control" id="myInput" type="text" placeholder="Buscar..">
  <br>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Correo</th>
        <th>Celular</th>
        <th>Programa</th>
        <th>Último valor pagado</th>
        <th>Estado</th>
      </tr>
    </thead>
    <tbody id="myTable">
      <tr>
      <?php GLOBAL $wpdb;
        foreach($result_user_active as $listar_usuarios){

            $nombre=$listar_usuarios->u_nombre;
            $apellido=$listar_usuarios->u_apellido;
            $correo=$listar_usuarios->u_correo_e;
            $celular=$listar_usuarios->u_celular;
            $programa=$listar_usuarios->p_nombre;
            $estado=$listar_usuarios->e_nombre;
            $ultimo_pago=$listar_usuarios->uxp_valor_mensual;
      
      ?>
        <td><?php echo $nombre;?></td>
        <td><?php echo $apellido;?></td>
        <td><?php echo $correo;?></td>
        <td><?php echo $celular;?></td>
        <td><?php echo $programa;?></td>
        <td><?php echo '$ '.$ultimo_pago;?></td>
        <?php
        
            if($estado=='Activo'){
              echo '<td style="background-color:#C2E1D5;">'.$estado.'</td>'; 
            }

            if($estado=='Inactivo'){
              echo '<td style="background-color:#E989B6;">'.$estado.'</td>'; 
            }
            
        ?>
        
      </tr>
      <?php }?>
    </tbody>
  </table>
  
</div>


