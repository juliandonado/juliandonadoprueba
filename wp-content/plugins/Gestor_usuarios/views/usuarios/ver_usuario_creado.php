<script>

// AJAX para modal -detalle del enrolamiento-

function detailsprograma(idenroll){
    mensaje = idenroll;
    
    var request = jQuery.ajax({
        url: "controlador/controller.php?metodo=enrolamientoLinea"+idenroll,
        method: "GET",
        
        dataType: "html"
    });
        
        request.done(function( msg ) {
            jQuery('#info_programa').html(msg);
        });
        
    request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + 'por el programa' );
    });
    jQuery('#ver_enrol').modal('show');

    //Se cambio el signo $ por jQuery para que funcionara en wordpress, tener en cuenta
    
}

</script>



<center> 
    <div class="container">
        <div class="container-fluid">
            <div class="d-inline-flex p-3 bg-secondary text-white">  
                <div class="p-2 bg-info">
                <?php
                foreach($result_ultimo_usuario_registrado as $usuario){
                    
                    echo " <h1> <svg class='bi bi-person-check-fill' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
                    <path fill-rule='evenodd' d='M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 100-6 3 3 0 000 6zm9.854-2.854a.5.5 0 010 .708l-3 3a.5.5 0 01-.708 0l-1.5-1.5a.5.5 0 01.708-.708L12.5 7.793l2.646-2.647a.5.5 0 01.708 0z' clip-rule='evenodd'/>
                </svg>
                El usuario de ".$usuario->u_nombre." ".$usuario->u_apellido." se ha creado exitosamente
                   </h1>";
                    $fecha_nacimiento = $usuario->u_fecha_nacimiento;
                    $yearB=[];
                    $yearB = explode ('-',$fecha_nacimiento);
                    
                
                ?>
                   
                    <p>Ahora podrá finalizar los procesos de inscripción y datos adicionales.</p>
                </div>
            </div>
        </div>
    </div>
</center>

<br>
<br>


        <div class="row">
            <div class="col">
        
                <!-- Button to Open the Modal -->
                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#myModal">
                    <svg class="bi bi-heart-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" clip-rule="evenodd"/>
                </svg>
                                Datos médicos
                </button>

                <!-- The Modal -->
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                    
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Datos médicos</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="conteiner">
                                    <center> 
                                            <div class="container-fluid">
                                                <div class="d-inline-flex p-3 bg-secondary text-white">  
                                                    <div class="p-2 bg-info">
                                                        <h1>Ingresar datos médicos</h1>
                                                        <p>Deberá ingresar toda la información médica solicitada en esta sección.</p>
                                                    </div>
                                                </div>
                                            </div>
                                    </center>
                                    <br><br>

                                    <center>
                                        <h1>
                                            <strong>
                                            <svg class="bi bi-heart-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" clip-rule="evenodd"/>
                                            </svg> DATOS MÉDICOS
                                            </strong>
                                        </h1>
                                    </center>
                                    <br><br>
                            
                                    <form action="controller/controlador.php?metodo=datosMedicos" name="datos_medicos" method="POST" novalidate>
                                    <?php $verlinea_usuario=$usuario->u_pk; ?>
                                    <input type="hidden" id="custId" name="usuario" value="<?php echo $verlinea_usuario; 
                                    ?>">
                                                                    
                                        <div class="form-group">
                                            <H4>EPS</H4>
                                            <select name="eps" require>
                                                <option value="s.u">---Seleccione uno---</option>

                                                <?php
                                                foreach($result_eps as $mieps){
                                                    echo "<option value='".$mieps->eps_pk."'>".$mieps->eps_nombre."</option>";
                                                }
                                                ?>
                                            
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <H4>Grupo Sanguineo</H4>
                                            <select name="g_sangre" require>
                                                <option value="s.u">---Seleccione uno---</option>
                                                <?php
                                                foreach($result_gsanguineo as $migs){
                                                    echo "<option value='".$migs->gs_pk."'>".$migs->gs_nombre."</option>";
                                                }
                                                ?>
                                                
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <H4>Anotaciones médicas</H4>
                                                <div class="alert alert-danger">
                                                    <strong>Importante!</strong> Indicar cualquier condición médica que pueda tener cualquier tipo de impacto fisico o mental dentro de las clases suministradas, de no registrar indicar NINGUNO.
                                                </div>
                                            <textarea name="anota_med"  style="background-color: #E0E5E4;" cols="93" rows="5"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label><strong><h4>Peso actual en KG</strong></h4></strong></label>
                                            <input type="number"  style="background-color: #E0E5E4;" class="form-control" name="peso" placeholder="Ingrese su peso en KG"  required>
                                        </div>

                                        <div class="form-group">
                                            <label><strong><h4>Estatura actual</strong></h4></strong></label>
                                            <input type="number"  style="background-color: #E0E5E4;" class="form-control" name="estatura" placeholder="Ingrese estatura actual"  required>
                                        </div>
                                    
                                        <button type="submit" class="btn btn-primary">Cargar datos médicos</button>
                                    </form>
                                    </div>
                                </div>
                                
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar ventana</button>
                            </div>
                    
                    </div>
                </div>
            </div>
        </div><!-- Cierra del div del conteniner del modal -->

    

        <div class="col">
    
            <!-- Button to Open the Modal -->
            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#myModal2">
            <svg class="bi bi-bag-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M1 4h14v10a2 2 0 01-2 2H3a2 2 0 01-2-2V4zm7-2.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
            </svg>
                    Inscribir programas
            </button>

            <!-- The Modal -->
            <div class="modal fade" id="myModal2">
                <div class="modal-dialog modal-lg">
                <div class="modal-content">
                
                    <!-- Modal Header -->
                    <div class="modal-header">
                    <h4 class="modal-title">Seleccione el programa</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="conteiner">
                            <center> 
                                <div class="container">
                                    <div class="container-fluid">
                                        <div class="d-inline-flex p-3 bg-secondary text-white">  
                                            <div class="p-2 bg-info">
                                                <h1>Ingresar programa a inscribir</h1>
                                                <p>Seleccione el programa y el rol del usuario.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </center>

                            <center><h1><strong><img style="max-width: 5%; max-height: 5%" src="http://localhost/expofighting/wp-content/uploads/2020/04/ficha.png" alt="">PROGRAMA</strong></h1></center>
                                <form action="controller/controlador.php?metodo=enrolamiento" name="enrolar" method="POST" novalidate>
                                
                                    <input type="hidden" id="custId" name="usuario" value="<?php echo $verlinea_usuario; 
                                    ?>">
                                                                    
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <H4>Programa</H4>
                                                    <select name="prog" require>
                                                        <option value="s.u">---Seleccione uno---</option>
                                                            <?php
                                                                foreach($result_prog as $programa){
                                                                    echo "<option value='".$programa->p_pk."'>".$programa->p_nombre."</option>";
                                                                }
                                                            ?>

                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="col">
                                                <div class="form-group">
                                                    <H4>Rol del usuario</H4>
                                                    <select name="rol" require>
                                                        <option value="s.u">---Seleccione uno---</option>
                                                        <?php
                                                            foreach($result_role as $rol){
                                                                echo "<option value='".$rol->rol_pk."'>".$rol->rol_NOMBRE."</option>";
                                                                }
                                                        ?>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="alert alert-primary">
                                            <strong>Verifique la información seleccionada </strong> Antes de finalizar el proceso de inscripción favor verificar los datos seleccionado.</a>.
                                        </div>

                                        
                                    
                                    <button type="submit" class="btn btn-primary">Cargar enrolamiento</button>
                                </form>
                            </div>
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar ventana</button>
                    </div>
                    
                </div>
                </div>
            </div>
        </div><!-- Cierra del div del conteniner del modal -->

    <div class="col">
            <!-- Button to Open the Modal -->
            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#myModal3">
                <svg class="bi bi-inboxes-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M.125 11.17A.5.5 0 01.5 11H6a.5.5 0 01.5.5 1.5 1.5 0 003 0 .5.5 0 01.5-.5h5.5a.5.5 0 01.496.562l-.39 3.124A1.5 1.5 0 0114.117 16H1.883a1.5 1.5 0 01-1.489-1.314l-.39-3.124a.5.5 0 01.121-.393zM3.81.563A1.5 1.5 0 014.98 0h6.04a1.5 1.5 0 011.17.563l3.7 4.625a.5.5 0 01-.78.624l-3.7-4.624A.5.5 0 0011.02 1H4.98a.5.5 0 00-.39.188L.89 5.812a.5.5 0 11-.78-.624L3.81.563z" clip-rule="evenodd"/>
                    <path fill-rule="evenodd" d="M.125 5.17A.5.5 0 01.5 5H6a.5.5 0 01.5.5 1.5 1.5 0 003 0A.5.5 0 0110 5h5.5a.5.5 0 01.496.562l-.39 3.124A1.5 1.5 0 0114.117 10H1.883A1.5 1.5 0 01.394 8.686l-.39-3.124a.5.5 0 01.121-.393z" clip-rule="evenodd"/>
                </svg>
                Asignar mensualidad
            </button>

            <!-- The Modal -->
            <div class="modal fade" id="myModal3">
                <div class="modal-dialog modal-lg">
                <div class="modal-content">
                
                    <!-- Modal Header -->
                    <div class="modal-header">
                    <h4 class="modal-title">Gestione Mensualidad</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    <!-- Modal body -->
                    <div class="modal-body">
                    
                            <div class="conteiner">
                                    <center> 
                                        <div class="container">
                                            <div class="container-fluid">
                                                <div class="d-inline-flex p-3 bg-secondary text-white">  
                                                    <div class="p-2 bg-info">
                                                        <h1>Asignar valor de la mensualidad</h1>
                                                        <p>Seleccione el paquete que corresponda o ingrese manualmente el valor y fecha de vencimiento.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </center>

                                    <br><br>

                                    <center><h1><strong>
                                    <svg class="bi bi-inboxes-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M.125 11.17A.5.5 0 01.5 11H6a.5.5 0 01.5.5 1.5 1.5 0 003 0 .5.5 0 01.5-.5h5.5a.5.5 0 01.496.562l-.39 3.124A1.5 1.5 0 0114.117 16H1.883a1.5 1.5 0 01-1.489-1.314l-.39-3.124a.5.5 0 01.121-.393zM3.81.563A1.5 1.5 0 014.98 0h6.04a1.5 1.5 0 011.17.563l3.7 4.625a.5.5 0 01-.78.624l-3.7-4.624A.5.5 0 0011.02 1H4.98a.5.5 0 00-.39.188L.89 5.812a.5.5 0 11-.78-.624L3.81.563z" clip-rule="evenodd"/>
                                        <path fill-rule="evenodd" d="M.125 5.17A.5.5 0 01.5 5H6a.5.5 0 01.5.5 1.5 1.5 0 003 0A.5.5 0 0110 5h5.5a.5.5 0 01.496.562l-.39 3.124A1.5 1.5 0 0114.117 10H1.883A1.5 1.5 0 01.394 8.686l-.39-3.124a.5.5 0 01.121-.393z" clip-rule="evenodd"/>
                                    </svg> PAQUETES DISPONIBLES</strong></h1></center>
                                    <table class="table table-hover table-bordered table-sm table-responsive">
                        <thead>
                        
                            <tr>
                                <?php
                                    foreach($result_paquete1 as $paquete_01){

                                        $nombrepq=$paquete_01->pq_nombre;

                                        if($paquete_01==1){

                                        echo'

                                        <th>'.$nombrepq.'<br>
                                            
                                        </th>';

                                        }
                                    }

                                ?>
                                <?php
                                    foreach($result_paquete2 as $paquete_02){

                                        $nombrepq=$paquete_02->pq_nombre;

                                        if($paquete_02==1){

                                        echo'

                                        <th>'.$nombrepq.'<br>
                                            
                                        </th>';

                                        }
                                    }

                                ?>
                                <?php
                                    foreach($result_paquete3 as $paquete_03){

                                        $nombrepq=$paquete_03->pq_nombre;

                                        if($paquete_03==1){

                                        echo'

                                        <th>'.$nombrepq.'<br>
                                            
                                        </th>';

                                        }
                                    }

                                ?>
                                <?php
                                    foreach($result_paquete4 as $paquete_04){

                                        $nombrepq=$paquete_04->pq_nombre;

                                        if($paquete_04==1){

                                        echo'

                                        <th>'.$nombrepq.'<br>
                                            
                                        </th>';

                                        }
                                    }

                                ?>
                                <?php
                                    foreach($result_paquete5 as $paquete_05){

                                        $nombrepq=$paquete_05->pq_nombre;

                                        if($paquete_05==1){

                                        echo'

                                        <th>'.$nombrepq.'<br>
                                            
                                        </th>';

                                        }
                                    }

                                ?>
                                <?php
                                    foreach($result_paquete6 as $paquete_06){

                                        $nombrepq=$paquete_06->pq_nombre;

                                        if($paquete_06==1){

                                        echo'

                                        <th>'.$nombrepq.'<br>
                                            
                                        </th>';

                                        }
                                    }

                                ?>
                                <?php
                                    foreach($result_paquete7 as $paquete_07){

                                        $nombrepq=$paquete_07->pq_nombre;

                                        if($paquete_07==1){

                                        echo'

                                        <th>'.$nombrepq.'<br>
                                        
                                        </th>';

                                        }
                                    }

                                ?>
                                <?php
                                    foreach($result_paquete8 as $paquete_08){

                                        $nombrepq=$paquete_08->pq_nombre;

                                        if($paquete_08==1){

                                        echo'

                                        <th>'.$nombrepq.'<br>
                                            
                                        </th>';

                                        }
                                    }

                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <?php  GLOBAL $wpdb ; 

                        foreach($result_paquete1 as $pq_1){

                            $pq_descripcion=$pq_1->pq_descripción;
                            $pq_activo=$pq_1->e_pk;
                            $pq_id=$pq_1->pq_id;

                            if($pq_activo==1){
                            echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                    }

                        foreach($result_paquete2 as $pq_2){

                            $pq_descripcion=$pq_2->pq_descripción;
                            $pq_activo=$pq_2->e_pk;
                            $pq_id=$pq_2->pq_id;

                            if($pq_activo==1){
                            echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            }
                    
                        foreach($result_paquete3 as $pq_3){

                                $pq_descripcion=$pq_3->pq_descripción;
                                $pq_id=$pq_3->pq_id;
                                $pq_activo=$pq_3->e_pk;

                                if($pq_activo==1){
                                    echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            } 

                        foreach($result_paquete4 as $pq_4){

                            $pq_descripcion=$pq_4->pq_descripción;
                            $pq_id=$pq_4->pq_id;
                            $pq_activo=$pq_4->e_pk;

                            if($pq_activo==1){
                                echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            } 
                        
                        foreach($result_paquete5 as $pq_5){

                            $pq_descripcion=$pq_5->pq_descripción;
                            $pq_id=$pq_5->pq_id;
                            $pq_activo=$pq_5->e_pk;

                            if($pq_activo==1){
                                echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            } 
                            
                        foreach($result_paquete6 as $pq_6){

                            $pq_descripcion=$pq_6->pq_descripción;
                            $pq_id=$pq_6->pq_id;
                            $pq_activo=$pq_6->e_pk;

                            if($pq_activo==1){
                                echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            } 
                            
                        foreach($result_paquete7 as $pq_7){
                            
                            $pq_descripcion=$pq_7->pq_descripción;
                            $pq_id=$pq_7->pq_id;
                            $pq_activo=$pq_7->e_pk;

                            if($pq_activo==1){
                                echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            }  
                        
                        foreach($result_paquete8 as $pq_8){

                            $pq_descripcion=$pq_8->pq_descripción;
                            $pq_id=$pq_8->pq_id;
                            $pq_activo=$pq_8->e_pk;

                            if($pq_activo==1){
                                echo '<td>'.$pq_descripcion.'<br><br><strong>ID de paquete</strong> '.$pq_id.'</td>'; }
                            } ?>                          
                    </tr>
                </tbody>
            </table>

                            <h1>Diligencia detalles del pauqete y pago mensual</h1>
                                <form action="controller/controlador.php?metodo=mensualidadxusuario" name="pago_usuario" method="POST" novalidate>
                                
                                    <input type="hidden" id="custId" name="usuario" value="<?php echo $verlinea_usuario; 
                                    ?>">
                                                                    
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <H4>Asigne paquete de servicios</H4>
                                                    <select name="pqt" require>
                                                        <option value="s.u">---Seleccione uno---</option>
                                                            <?php
                                                                foreach($result_paquetes as $paquete){
                                                                    echo "<option value='".$paquete->pq_pk."'>".$paquete->pq_nombre."</option>";
                                                                }
                                                                
                                                            ?>

                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <!-- <div class="col">
                                                <div class="form-group">
                                                    <H4>Puede asignar el valor manualmente</H4>
                                                    <input type="text" name="valor_manual" placeholder="Ingrese el valor en pesos" >
                                                    <br><br>
                                                    <div class="alert alert-info">
                                                        <strong>Información importante </strong> El valor ingresado manualmente sobreescribirá el valor del paquete de servicios. </a>
                                                    </div>
                                                </div>
                                            </div> -->
                                            
                                            <div class="col">
                                                <div class="form-group">
                                                    <H4>Seleccione el estado del pago</H4>
                                                    <select name="estado_pago" require>
                                                        <option value="s.u">---Seleccione uno---</option>
                                                        <?php
                                                            foreach($result_estado as $estado){
                                                                echo "<option value='".$estado->e_pk."'>".$estado->e_nombre."</option>";
                                                                }
                                                        ?>

                                                    </select>

                                                    <br><br>
                                                    <div class="alert alert-warning">
                                                        <strong>Recuerde!!  </strong> que si el pago queda pendeinte puede finalizarlo mas adelante, despues de terminar la inscripción. </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="alert alert-primary">
                                            <strong>Verifique la información seleccionada </strong> antes de finalizar el proceso. </a>
                                        </div>
                                        
                                    
                                    <button type="submit" class="btn btn-primary">Cargar pago</button>
                                </form>
                            </div>

                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    
                </div>
                </div>
            </div>
        </div><!-- Cierra del div del conteniner del modal -->

            <div class="col">
            <?php
            $edad = 2020 - $yearB[0];
               
            if($edad<18){
                
                echo "
                <button type='button' class='btn btn-warning' data-toggle='modal' data-target='#myModal' style='align:center;'>
                    <svg class='bi bi-shield-shaded' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
                        <path fill-rule='evenodd' d='M5.443 1.991a60.17 60.17 0 00-2.725.802.454.454 0 00-.315.366C1.87 7.056 3.1 9.9 4.567 11.773c.736.94 1.533 1.636 2.197 2.093.333.228.626.394.857.5.116.053.21.089.282.11A.73.73 0 008 14.5c.007-.001.038-.005.097-.023.072-.022.166-.058.282-.111.23-.106.525-.272.857-.5a10.197 10.197 0 002.197-2.093C12.9 9.9 14.13 7.056 13.597 3.159a.454.454 0 00-.315-.366c-.626-.2-1.682-.526-2.725-.802C9.491 1.71 8.51 1.5 8 1.5c-.51 0-1.49.21-2.557.491zm-.256-.966C6.23.749 7.337.5 8 .5c.662 0 1.77.249 2.813.525a61.09 61.09 0 012.772.815c.528.168.926.623 1.003 1.184.573 4.197-.756 7.307-2.367 9.365a11.191 11.191 0 01-2.418 2.3 6.942 6.942 0 01-1.007.586c-.27.124-.558.225-.796.225s-.526-.101-.796-.225a6.908 6.908 0 01-1.007-.586 11.192 11.192 0 01-2.417-2.3C2.167 10.331.839 7.221 1.412 3.024A1.454 1.454 0 012.415 1.84a61.11 61.11 0 012.772-.815z' clip-rule='evenodd'/>
                        <path d='M8 2.25c.909 0 3.188.685 4.254 1.022a.94.94 0 01.656.773c.814 6.424-4.13 9.452-4.91 9.452V2.25z'/>
                    </svg>

                Inscribir acudiente
                </button>
                
                ";
            }
            

?>
            <!-- Button to Open the Modal -->
           

            <!-- The Modal -->
            <div class="modal fade" id="myModal">
                <div class="modal-dialog modal-xl">
                <div class="modal-content">
            
                <!-- Modal Header -->
                <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                Modal body..
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
            </div>
        </div>
        </div><!-- Cierra del div del conteniner del modal -->
    
</div>


<br><br>

<div class="container">
  <h2><svg class="bi bi-people-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z"/>
  <path fill-rule="evenodd" d="M8 9a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>
  <path fill-rule="evenodd" d="M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z" clip-rule="evenodd"/>
</svg>
  <path fill-rule="evenodd" d="M14 1H2a1 1 0 00-1 1v12a1 1 0 001 1h12a1 1 0 001-1V2a1 1 0 00-1-1zM2 0a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2H2z" clip-rule="evenodd"/>
  <path fill-rule="evenodd" d="M2 15v-1c0-1 1-4 6-4s6 3 6 4v1H2zm6-6a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>
</svg>  Resumen de los datos personales del usuario</h2>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Documento de identidad</th>
            <th>Fecha de nacimiento <br> (AAAA/MM/DD)</th>
            <th>Celular</th>
            <th>Fijo</th>
            <th>correo</th>
            <th>Dirección</th>
            <th>Ciudad</th>
            <th>País</th>
        </tr>
        </thead>
        <tbody>
        <tr style="color:black; text-align: center;">
            <td><?php echo $usuario->u_nombre;?></td>
            <td><?php echo $usuario->u_apellido; ?></td>
            <td><?php echo $usuario->u_documento.$usuario->td_abrev; ?></td>
            <td><?php echo $usuario->u_fecha_nacimiento; ?></td>
            <td><?php echo $usuario->u_celular; ?></td>
            <td><?php echo $usuario->u_fijo; ?></td>
            <td><?php echo $usuario->u_correo_e; ?></td>
            <td><?php echo $usuario->u_direccion; ?></td>
            <td><?php echo $usuario->u_ciudad; ?></td>
            <td><?php echo $usuario->u_pais; ?></td>
        </tr>
        </tbody>
    </table>
    </div>                      



    <div class='row'>
        <div class='col-sm-2 col-md-6'>
                <h2>
                    <svg class="bi bi-heart-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" clip-rule="evenodd"/>
                    </svg> Resumen de los datos médicos del usuario
                </h2>
                <table class='table table-hover'>
                    <thead>
                        <tr style='color:black; text-align: center;'>
                            <th>EPS</th>
                            <th>Tipo de Sangre</th>
                            <th>Notas médicas</th>
                            <th>Peso actual</th>
                            <th>Estarura actual</th>
                            <th>PAR Q & YOU</th>
                            <th>Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                            GLOBAL $wpdb;

                                foreach($result_ver_dato_medico as $resultado_user_medico){
                                    $eps=$resultado_user_medico->eps_nombre;
                                    $grupo_sanguineo=$resultado_user_medico->gs_nombre; 
                                    $nota_medica=$resultado_user_medico->dm_nota_med;
                                    $peso=$resultado_user_medico->dm_peso ;
                                    $estatura=$resultado_user_medico->dm_estatura;
                                

                            //            echo $wpdb->last_query;
                            // echo $wpdb->last_result;
                            // echo $wpdb->last_error;
                            
                            ?>
                        <tr style='color:black;  text-align: center;'>
                            
                        <?php }  ?>
                                <td><?php echo $eps; ?></td>
                                <td><?php echo $grupo_sanguineo; ?></td>
                                <td style='text-align:justify;'><?php  echo $nota_medica; ?></td>
                                <td><?php  echo $peso; ?></td>
                                <td><?php  echo $estatura;   ?></td>

                               
                               
                                <td> <!-- Button to Open the Modal -->
                                    <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#parq'>
                                        <svg class="bi bi-envelope-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z"/>
                                    </svg>
                                    </button>
                                </td>
                                
                            
                                
                                <td> <!-- Button to Open the Modal -->
                                    <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#myModaledit1'>
                                        <svg class='bi bi-pencil-square' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
                                            <path d='M15.502 1.94a.5.5 0 010 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 01.707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 00-.121.196l-.805 2.414a.25.25 0 00.316.316l2.414-.805a.5.5 0 00.196-.12l6.813-6.814z'/>
                                        <path fill-rule='evenodd' d='M1 13.5A1.5 1.5 0 002.5 15h11a1.5 1.5 0 001.5-1.5v-6a.5.5 0 00-1 0v6a.5.5 0 01-.5.5h-11a.5.5 0 01-.5-.5v-11a.5.5 0 01.5-.5H9a.5.5 0 000-1H2.5A1.5 1.5 0 001 2.5v11z' clip-rule='evenodd'/>
                                        </svg>
                                    </button>
                                </td>
                                
                            </tr>
                            
                           
                        </tbody>
                        
                    </table>
                    
                </div>
                                
                <!-- The Modal para enviar el PAR Q & YOU -->
                <div class="modal" id="parq">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    
                        <!-- Modal Header -->
                        <div class="modal-header">
                        <h4 class="modal-title">Encuesta PAR Q & YOU </h4>
                        
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="modal-body">
                        <p style="color:red;">Nombre:</p>
                            <h6>
                                <strong>
                                    <?php 

                                    GLOBAL $wpdb;
                                        echo $verlinea_usuario=$usuario->u_nombre.' '.$verlinea_usuario=$usuario->u_apellido ;

                                        // print_r($result_ver_dato_medico);

                                        foreach($result_ver_dato_medico as $validarpq){

                                           

                                            $parq=$validarpq->pq_pk;
                                            if($parq==NULL){
                                                echo '<div class="alert alert-warning">
                                                <strong>No ha dado respuesta a la encuesta!</strong> '.$verlinea_usuario=$usuario->u_nombre.' '.$verlinea_usuario=$usuario->u_apellido.'Se encuentra pendiente realizar la encuesta PAR Q & YOU
                                              </div>';
                                            }
                                        }
                                    ?>
                                </strong>
                            </h6>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                        
                    </div>
                    </div>
                </div>
  

                <!-- The Modal para actualizar los datos médicos-->
                <div class="modal fade" id="myModaledit1">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                    
                      <!-- Modal Header -->
                      <div class="modal-header">
                        <h4 class="modal-title"><svg class='bi bi-heart-fill' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
                            <path fill-rule='evenodd' d='M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z' clip-rule='evenodd'/>
                            </svg>  Datos médico de <?php echo $verlinea_usuario=$usuario->u_nombre.' '.$verlinea_usuario=$usuario->u_apellido ;?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>

                     <div class="conteiner">
                     <br>
                        <h6>
                            <svg class="bi bi-info-circle-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 16A8 8 0 108 0a8 8 0 000 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>
                            </svg>
                            Datos actualizados por última vez el <span class="badge badge-secondary"><?php echo $resultado_user_medico->dm_fecha_creacion;?></span>
                        </h6>
                     </div>
                      
                      <!-- Modal body -->
                    <div class="modal-body">
                            <div class="alert alert-warning">
                                <strong>Atenciòn!</strong> Desde acá podrá cambiar los datos médicos del usuario. <strong>Verifique bien los datos que desea modificar.</strong>
                            </div>
                            
                            <form action="controller/controlador.php?metodo=edit_datosMedicos" name="editar_datos_medicos" method="POST" novalidate>
                                <?php $verlinea_usuario=$usuario->u_pk; ?>
                                <input type="hidden" name="usuario" value="<?php echo $verlinea_usuario; 
                                    ?>">                       
                                    <div class="form-group">
                                        <h4>EPS</h4>
                                        <div class="row">
                                            <div class="col" >
                                                <div class="alert alert-secondary">
                                                    <strong>Dato anterior</strong> (<?php echo $resultado_user_medico->eps_nombre; ?>)
                                                </div>
                                            </div>
                                            <div class="col" >
                                                <div class="alert alert-info">
                                                    <strong>Cambiar a</strong> 
                                                        <select name="eps" require>
                                                            <option value="s.u">---Seleccione uno---</option>

                                                                <?php
                                                                    foreach($result_eps as $mieps){
                                                                        echo "<option value='".$mieps->eps_pk."'>".$mieps->eps_nombre."</option>";
                                                                    }
                                                                    ?>         
                                                        </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="form-group">
                                        <H4>Grupo Sanguineo</H4>

                                        <div class="row">
                                            <div class="col" >
                                                <div class="alert alert-secondary">
                                                        <strong>Dato anterior</strong> (<?php echo $resultado_user_medico->gs_nombre; ?>)
                                                </div>
                                            </div>
                                            <div class="col" >
                                                <div class="alert alert-info">
                                                        <strong>Cambiar a</strong> 
                                                        <select name="g_sangre" require>
                                                            <option value="s.u">---Seleccione uno---</option>
                                                            <?php
                                                            foreach($result_gsanguineo as $migs){
                                                                echo "<option value='".$migs->gs_pk."'>".$migs->gs_nombre."</option>";
                                                            }
                                                            ?>
                                                            
                                                        </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="form-group">
                                        <h4>Peso actual en KG</h4>
                                        <div class="row">
                                            <div class="col" >
                                                <div class="alert alert-secondary">
                                                        <strong>Dato anterior</strong> (<?php echo $resultado_user_medico->dm_peso; ?>)
                                                </div>
                                            </div>
                                            <div class="col" >
                                                <div class="alert alert-info">
                                                        <strong>Cambiar a <input type="number"  style="background-color: #E0E5E4;" class="form-control" name="peso" placeholder="Ingrese su peso en KG"  required>
                                                        </strong> 
                                                        
                                                        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="form-group">
                                        <h4>Estatura actual</h4>
                                        <div class="row">
                                            <div class="col" >
                                                <div class="alert alert-secondary">
                                                    <strong>Dato anterior</strong> (<?php echo $resultado_user_medico->dm_estatura; ?>)
                                                </div>
                                            </div>
                                            <div class="col" >
                                                <div class="alert alert-info">
                                                        <strong>Cambiar a <input type="number"  style="background-color: #E0E5E4;" class="form-control" name="estatura" placeholder="Ingrese estatura actual"  required></strong> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <h4>Notas médicas</h4>
                                            <div class="alert alert-secondary">
                                                <strong>Dato anterior</strong> <br>
                                                    (<?php echo $resultado_user_medico->dm_nota_med; ?>)
                                            </div>

                                            <div class="alert alert-info">
                                                <strong>
                                                    Cambiar a 
                                                    <textarea name="anota_med"  style="background-color: #E0E5E4;" cols="113" rows="5">
                                                    </textarea> 
                                                </strong>
                                            </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary">
                                        <svg class="bi bi-arrow-clockwise" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M3.17 6.706a5 5 0 017.103-3.16.5.5 0 10.454-.892A6 6 0 1013.455 5.5a.5.5 0 00-.91.417 5 5 0 11-9.375.789z" clip-rule="evenodd"/>
                                            <path fill-rule="evenodd" d="M8.147.146a.5.5 0 01.707 0l2.5 2.5a.5.5 0 010 .708l-2.5 2.5a.5.5 0 11-.707-.708L10.293 3 8.147.854a.5.5 0 010-.708z" clip-rule="evenodd"/>
                                        </svg>
                                        Actualizar datos médicos
                                    </button>
                            </form>
                    </div>
                      
                      <!-- Modal footer -->
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar Ventana</button>
                      </div>
                    </div>
                  </div>
                </div>


                <!-- TABLA DE ENROLAMEINTOS -->

                <?php
                

               $tabla_eroll="<div class='col-sm-2 col-md-6'>
                <h2><svg class='bi bi-briefcase-fill' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
                    <path fill-rule='evenodd' d='M0 12.5A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5V6.85L8.129 8.947a.5.5 0 01-.258 0L0 6.85v5.65z' clip-rule='evenodd'/>
                    <path fill-rule='evenodd' d='M0 4.5A1.5 1.5 0 011.5 3h13A1.5 1.5 0 0116 4.5v1.384l-7.614 2.03a1.5 1.5 0 01-.772 0L0 5.884V4.5zm5-2A1.5 1.5 0 016.5 1h3A1.5 1.5 0 0111 2.5V3h-1v-.5a.5.5 0 00-.5-.5h-3a.5.5 0 00-.5.5V3H5v-.5z' clip-rule='evenodd'/>
                  </svg> Resumen de los programas inscritos del usuario</h2>
                <table class='table table-hover'>
                <thead>
                    <tr style='color:black; text-align: center;'>
                        <th>Programas inscritos</th>
                        <th>Rol</th>
                        <th>Estado del enrolamiento</th>
                        <th>Fecha de enrolamiento</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style='color:black;  text-align: center;'>";
                
            foreach($result_last_enrol as $userxprograma){
                $pkenrolamiento=$userxprograma->er_pk;

                $tabla_eroll.= "
                        <td>".$userxprograma->p_nombre."</td>
                        <td>".$userxprograma->rol_NOMBRE." </td>
                        <td>".$userxprograma->e_nombre."</td>
                        <td>".$userxprograma->er_fecha_creacion."</td>
                        <td><a class='btn btn-primary' style=' color:white;' onclick='detailsprograma".$pkenrolamiento."'> <svg class='bi bi-pencil-square' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
                        <path d='M15.502 1.94a.5.5 0 010 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 01.707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 00-.121.196l-.805 2.414a.25.25 0 00.316.316l2.414-.805a.5.5 0 00.196-.12l6.813-6.814z'/>
                        <path fill-rule='evenodd' d='M1 13.5A1.5 1.5 0 002.5 15h11a1.5 1.5 0 001.5-1.5v-6a.5.5 0 00-1 0v6a.5.5 0 01-.5.5h-11a.5.5 0 01-.5-.5v-11a.5.5 0 01.5-.5H9a.5.5 0 000-1H2.5A1.5 1.5 0 001 2.5v11z' clip-rule='evenodd'/>
                      </svg></a></td>  
                    </tr>";

                
        }
        $tabla_eroll.="
                                </tbody>
                            </table>

                            <!-- The Modal -->
                            <div id='ver_enrol' class='modal' tabindex='-2' role='dialog' aria-labelledby='myLargeModalLabel' aria-hidden='true'>
                            <div class='modal-dialog modal-lg'>
                                <div class='modal-content'>
                                
                                <!-- Modal Header -->
                                <div class='modal-header'>
                                    <h4 class='modal-title' id='exampleModalLabel2'>Editar enrolamiento".$userxprograma->p_nombre."</h4>
                                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class='modal-body' id='info_programa'>
                                                
                                        
                                </div>
                                
                                <!-- Modal footer -->
                                <div class='modal-footer'>
                                    <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar ventana</button>
                                </div>
                                
                                </div>
                            </div>
                            </div>

                            
                        </div>
                    </div>";
        echo $tabla_eroll;
        
        
    
}//CIERRE DEL FOREACH PRINCIPAL



                
?>

<h1>Información de la mensualidad.</h1>

    <div class="container">         
        <table class="table table-hover table-bordered table-striped" >
            <thead>
            <tr>
                <th>Paquete seleccionado</th>
                <th>Valor matrícula</th>
                <th>Valor mensualidad</th>
                <th>Estado del pago</th>
                <th>Fecha próximo pago</th>
            </tr>
            </thead>
            <tbody>
            <tr>

            <?php
            foreach($result_ver_pago as $pago_mens){
                $paquete=$pago_mens->pq_nombre;
                $v_matricula=$pago_mens->uxp_valor_matricula;
                $v_mensualidad=$pago_mens->uxp_valor_mensual;
                $estado=$pago_mens->e_nombre;
                $prox_pago=$pago_mens->uxp_fecha_creacion;

            }
            
            ?>
                <td  style="vertical-align:middle;"> <?php echo $paquete;?></td>
                <td style="vertical-align:middle;">$ <?php echo $v_matricula;?></td>
                <td style="vertical-align:middle;">$ <?php echo 'quitar'. $v_mensualidad ;?> <!-- Button to Open the Modal -->
                    <button type='button' class='btn btn-primary' data-toggle='modal' data-target='#edit_mensualidad'>
                        <svg class='bi bi-pencil-square' width='1em' height='1em' viewBox='0 0 16 16' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
                            <path d='M15.502 1.94a.5.5 0 010 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 01.707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 00-.121.196l-.805 2.414a.25.25 0 00.316.316l2.414-.805a.5.5 0 00.196-.12l6.813-6.814z'/>
                            <path fill-rule='evenodd' d='M1 13.5A1.5 1.5 0 002.5 15h11a1.5 1.5 0 001.5-1.5v-6a.5.5 0 00-1 0v6a.5.5 0 01-.5.5h-11a.5.5 0 01-.5-.5v-11a.5.5 0 01.5-.5H9a.5.5 0 000-1H2.5A1.5 1.5 0 001 2.5v11z' clip-rule='evenodd'/>
                        </svg>
                    </button></td>
                    
                <?php if($estado='Pendiente por pago'){
                    echo '<td style="background-color: #F4DDAA; style="vertical-align:middle;"">'. $estado.'</td>';
                }else{
                    echo '<td style="background-color: #C1E2B5; style="vertical-align:middle;"">'. $estado.'</td>';
                } ?>
                <td style="vertical-align:middle;"><?php echo  date("d-m-Y",strtotime($prox_pago."+ 1 month")) ;?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>
    <br>
    <br>

     <!-- The Modal -->
     <div id='edit_mensualidad' class='modal' tabindex='-2' role='dialog' aria-labelledby='myLargeModalLabel' aria-hidden='true'>
                            <div class='modal-dialog modal-lg'>
                                <div class='modal-content'>
                                
                                <!-- Modal Header -->
                                <div class='modal-header'>
                                    <h4 class='modal-title' id='exampleModalLabel2'>Asignar mensualidad manualmente</h4>
                                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class='modal-body' id='info_programa'>
                                <div class="alert alert-warning">
                                <strong>Atenciòn!</strong> Desde acá podrá modificar el valor de la mensualidad ingresando el valor manualmente.
                            </div>
                            
                            <form action="controller/controlador.php?metodo=edit_usuario_mensualidad" name="editar_datos_medicos" method="POST" novalidate>
                                <?php $verlinea_usuario=$usuario->u_pk; ?>
                                <input type="hidden" name="usuario" value="<?php echo $verlinea_usuario; 
                                    ?>">                       
                                    <div class="form-group">
                                        <h4>Valor de la mensualidad</h4>
                                        <div class="row">
                                            <div class="col" >
                                                <div class="alert alert-secondary">
                                                    <strong>Dato anterior</strong> ($ <?php echo $v_mensualidad; ?>)
                                                </div>
                                            </div>
                                            <div class="col" >
                                                <div class="alert alert-info">
                                                    <strong>Cambiar a</strong> 
                                                    <input type="number"  style="background-color: #E0E5E4;" class="form-control" name="nuevo_valor" placeholder="Ingrese el nuevo valor"  required>
                                                       
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <button type="submit" class="btn btn-primary">
                                        <svg class="bi bi-arrow-clockwise" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M3.17 6.706a5 5 0 017.103-3.16.5.5 0 10.454-.892A6 6 0 1013.455 5.5a.5.5 0 00-.91.417 5 5 0 11-9.375.789z" clip-rule="evenodd"/>
                                            <path fill-rule="evenodd" d="M8.147.146a.5.5 0 01.707 0l2.5 2.5a.5.5 0 010 .708l-2.5 2.5a.5.5 0 11-.707-.708L10.293 3 8.147.854a.5.5 0 010-.708z" clip-rule="evenodd"/>
                                        </svg>
                                        Actualizar Mensualidad
                                    </button>
                            </form>
                    </div>
                                
                                <!-- Modal footer -->
                                <div class='modal-footer'>
                                    <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar ventana</button>
                                </div>
                                
                                </div>
                            </div>
                            </div>

                            
                        </div>
                    </div>

<a href="http://localhost/expofighting/inscribir-usuario/" class="btn btn-primary">
    <svg class="bi bi-person-plus-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm7.5-3a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
        <path fill-rule="evenodd" d="M13 7.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0v-2z"/>
    </svg> Incribir otro usuario
</a>




                      

