<script>

// AJAX para modal -detalle del enrolamiento-

function detailsprogram(idenroll){
    mensaje = idenroll;
    
    var request = $.ajax({
        url: "controlador/controller.php?metodo=enrolamientoLinea"+idenroll,
        method: "GET",
        
        dataType: "html"
    });
        
        request.done(function( msg ) {
        $('#info_programa').html(msg);
        });
        
    request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + 'por el programa' );
    });
    $('#ver_enrol').modal('show');
    
}

</script>



<center> 
    <div class="container">
        <div class="container-fluid">
            <div class="d-inline-flex p-3 bg-secondary text-white">  
                <div class="p-2 bg-info">
                    <h1>
                        <svg class="bi bi-person-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>
                        </svg>
                    EDITAR USUARIO</h1>
                    <p>Edite la información personal del usuario seleccionado.</p>
                </div>
            </div>
        </div>
    </div>
</center>
<br>

<h2>
    <svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clip-rule="evenodd"/>
        <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clip-rule="evenodd"/>
    </svg>

    Buscar Usuario
</h2>
  <p>Puede buscar por varios o un solo campo la información del usuario.</p> 

  <form action="controller/controlador.php?metodo=editer_user" name="editar_usuario" method="POST" novalidate>
<hr>
    <div class="row">
       
        <div class="col-sm-4" >
            <label> Buscar por nombre o apellido:</label>
            <input type="text"  placeholder=" Ingrese el nombre" name="person_name"> <br><br>
            <input type="text"  placeholder=" Ingrese el apellido" name="person_lstname">
        </div>

        <div class="col-sm-4" >
            <label> Buscar por número de documento:</label>
            <input type="text"  placeholder=" Ingrese el documento" name="person_doc">
        </div>
        
        <div class="col-sm-4" >
            <label> Buscar por Correo:</label> <br>
            <input type="text"  placeholder=" Ingrese el correo" name="person_email">
        </div>
    </div>
    <hr>
    <br>
    <button type="submit" class="btn btn-primary" style="color:white;">    
        <svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clip-rule="evenodd"/>
            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clip-rule="evenodd"/>
        </svg>        
    </button>
                                    
  </form> 


  
<div class="container mt-3">
    <p>Si desaa buscar algún usuario en particular puede buscar por cualqueir dato visualizado en la siguiente tabla</p>  
    <input class="form-control" id="myInput" type="text" placeholder="Buscar..">
    <br><br>
    <table class="table table-bordered table-striped table-hover">
      <thead style="text-align: center;">
        <tr>
          <th>ID principal</th>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Documento</th>
          <th>Correo</th>
          <th>Editar</th>
        </tr>
      </thead>
      <tbody id="myTable" >

      <?php
      GLOBAL $wpdb;

      isset($result_buscar_usuario);
      
    if(is_array($result_buscar_usuario)){

    

      foreach($result_buscar_usuario as $usuarioencontrado){

      
        $pk=$usuarioencontrado->u_pk;
        $nombre=$usuarioencontrado->u_nombre;
        $apellido=$usuarioencontrado->u_apellido;
        $doc=$usuarioencontrado->u_documento;
        $correo=$usuarioencontrado->u_correo_e;

        
      
      ?>

      <tr>
        <td><?php echo  $pk;?></td>
        <td><?php echo  $nombre;?></td>
        <td><?php echo  $apellido;?></td>
        <td><?php echo  $doc;?></td>
        <td><?php echo  $correo;?></td>
        <td><center><button class="btn btn-warning" style="align: center;">
        <svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.502 1.94a.5.5 0 010 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 01.707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 00-.121.196l-.805 2.414a.25.25 0 00.316.316l2.414-.805a.5.5 0 00.196-.12l6.813-6.814z"/>
            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 002.5 15h11a1.5 1.5 0 001.5-1.5v-6a.5.5 0 00-1 0v6a.5.5 0 01-.5.5h-11a.5.5 0 01-.5-.5v-11a.5.5 0 01.5-.5H9a.5.5 0 000-1H2.5A1.5 1.5 0 001 2.5v11z" clip-rule="evenodd"/>
        </svg>
        </button></center></td>
        </center></td>
      </tr>
      <?php
      }
    }
      ?>
    </tbody>
  </table>
</div>

