<div class="conteiner">
<center> 
    <div class="container">
        <div class="container-fluid">
            <div class="d-inline-flex p-3 bg-secondary text-white">  
                <div class="p-2 bg-info">
                    <h1>
                    <svg class="bi bi-people-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 008 15a6.987 6.987 0 005.468-2.63z"/>
                        <path fill-rule="evenodd" d="M8 9a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"/>
                        <path fill-rule="evenodd" d="M8 1a7 7 0 100 14A7 7 0 008 1zM0 8a8 8 0 1116 0A8 8 0 010 8z" clip-rule="evenodd"/>
                    </svg>
                    FORMULARIO PARA INSCRIPCIÓN DE USUARIOS</h1>
                    <p>Desde acá podrá crear un nuevo usuario en los programas actuales como estudiante o como instructor.</p>
                </div>
            </div>
        </div>
    </div>
</center>

<br>
<center><h1><strong>

 DATOS PERSONALES</strong></h1></center>
    
    <form action="controlador/controller.php?metodo=ingresar_usuario" name="nuevo_usuario" method="POST" novalidate>
        <div class="form-group">
            <label><strong><h4><strong>Nombre</strong></strong></h4></strong></label>
            <input  style="background-color: #E0E5E4;" type="text" class="form-control" name="nombre" placeholder="Nombre del nuevo usuario"  required>
        </div>

        <div class="form-group">
            <label><strong><h4>Apellido</strong></h4></strong></label>
            <input type="text"  style="background-color: #E0E5E4;" class="form-control" name="apellido" placeholder="Apellido del nuevo usuario"  required>
        </div>

        <div class="form-group">
            <label><strong><h4>Número de Documento</strong></h4></strong></label>
            <input type="number"  style="background-color: #E0E5E4;" class="form-control" name="num_doc" placeholder="ingrese el número del documento"  required>
        </div>

        <div class="form-group">
            <label><strong><h4>Tipo de documento</strong></h4></strong></label>
            <select name="tipo_doc" require>
              <option value="s.u">---Seleccione uno---</option>

              <?php
              GLOBAL $wpdb;
              foreach($result_tipodoc as $tipodocumento){

               

                echo "<option value='".$tipodocumento->td_pk."'>".$tipodocumento->td_nombre."</option>";

              }
              
              ?>
            </select>
        </div>

        <div class="form-group">
            <label><strong><h4>Fecha de nacimiento</strong></h4></strong></label>
            <input type="date"  style="background-color: #E0E5E4;" class="form-control" name="nacido" placeholder="Apellido del nuevo usuario" ame="uname" required>
        </div>        

        <div class="form-group">
            <label><strong><h4>Celular</strong></h4></strong></label>
            <input type="number" style="background-color: #E0E5E4;"  class="form-control" name="celular" placeholder="Ingrese número de celular"  >
        </div>

        <div class="form-group">
            <label><strong><h4>Teléfono fijo</strong></h4></strong></label>
            <input type="number" style="background-color: #E0E5E4;" class="form-control" name="fijo" placeholder="Ingrese número local"  >
        </div>

        <div class="form-group">
            <label><strong><h4>Correo electrónico</strong></h4></strong></label>
            <input type="mail"  style="background-color: #E0E5E4;" class="form-control" name="mail" placeholder="ingrese correo electrónico"  required>
        </div>

        <div class="form-group">
            <label><strong><h4>Dirección de residencia</strong></h4></strong></label>
            <input type="text"  style="background-color: #E0E5E4;" class="form-control" name="direcc" placeholder="Dirección de vivienda"  required>
        </div>
        <div class="form-group">
            <label><strong><h4>Pais</strong></h4></strong></label>
            <input type="text"  style="background-color: #E0E5E4;" class="form-control" name="pais" placeholder="Ingrese el país de Origen"  required>
        </div>

        <div class="form-group">
            <label><strong><h4>Cuidad</strong></h4></strong></label>
            <input type="text"  style="background-color: #E0E5E4;" class="form-control" name="ciudad" placeholder="Ingrese la ciudad"  required>
        </div>

        
        <button type="submit" class="btn btn-primary">
            <svg class="bi bi-person-check-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 100-6 3 3 0 000 6zm9.854-2.854a.5.5 0 010 .708l-3 3a.5.5 0 01-.708 0l-1.5-1.5a.5.5 0 01.708-.708L12.5 7.793l2.646-2.647a.5.5 0 01.708 0z" clip-rule="evenodd"/>
            </svg>
            Crear Usuario
        </button>
    </form>
</div>