
<div class="jumbotron text-center">
  <h1>Exploración Morriz</h1>
  <p>Trae a tu hijo a nuestras clases para ayudarlo a fortalecer su cuerpo y su mente, con personal calificado. (El programa va dirigido a niños de 2 1/2 a 6 años.) </p> 
</div>

<?php
echo do_shortcode('[smartslider3 slider=3]');
?>



<h1>Cómo lo hacemos?</h1>

<p> Cuando estimulamos a nuestros pequeños les estamos presentando diferentes oportunidades para explorar, adquirir destrezas y habilidades de manera natural, buscando fortalecer la comprensión del entorno y sus contingencias.</p>

<p>La necesidad de movimiento en los niños, debe estar orientada para potencializar el desarrollo de sus capacidades físicas y de coordinación, mediante la implementación ajustada del movimiento en sus innumerables formas de aplicación.
</p>

<h1>Horarios</h1>

<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>LUNES</th>
            <th>MARTES</th>
            <th>MIÉRCOLES</th>
            <th>JUEVES</th>
            <th>VIERNES</th>
            <th>SABADO</th>
            <th>DOMINGO</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>5:00 pm</td>
            <td></td>
            <td>5:00 pm</td>
            <td></td>
            <td>5:00 pm</td>
            <td>9:00 am</td>
            <td>10:00 am</td>
        </tr>
        </tbody>
    </table>
</div>

<h1>Galería de imágenes</h1>
<?php
echo do_shortcode('[smartslider3 slider=2]');
?>