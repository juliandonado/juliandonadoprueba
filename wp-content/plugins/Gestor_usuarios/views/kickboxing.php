
<div class="jumbotron text-center">
  <h1>KICKBOXING</h1>
  <p>Con la práctica del kick boxing forjamos un espíritu competitivo por medio de artes marciales, buscamos que las personas que se encuentran en este programa tengan una manejo de su tiempo libre adecuado facilitando así su desarrollo social enmarcado en un deporte de contacto.</p> 
</div>

<h1>Galería De Imágenes</h1>

<?php
echo do_shortcode('[smartslider3 slider=5]');
?>

<h1>KickBoxing Ejecutivo</h1>

<p>Nuestro objetivo es formar a las personas y guiarlas a mantenerse sanas  no solo en su exterior, también en su interior, por lo cual manejamos el programa con clases llenas de retos que incluyen entrenamiento de alta intensidad, crossfit, kick boxing y entrenamiento funcional, en Expofighting buscamos una manera diferente de trabajar todo el cuerpo para cambiar la rutina del día a día.</p>

<h1>Horarios</h1>

<h4>KickBoxing Ejecutivo</h4>


<div class="container">
<table class="table table-hover">
        <thead>
        <tr>
            <th>OPCIÓN</th>
            <th>LUNES</th>
            <th>MARTES</th>
            <th>MIÉRCOLES</th>
            <th>JUEVES</th>
            <th>VIERNES</th>
            <th>SABADO</th>
            <th>DOMINGO</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>6:00 pm</td>
            <td></td>
            <td>6:00 pm</td>
            <td></td>
            <td>6:00 pm</td>
            <td>10:00 am</td>
            <td>9:00 am</td>
        </tr>
        <tr>
            <td>2</td>
            <td>8:00 pm</td>
            <td>8:00 pm</td>
            <td>8:00 pm</td>
            <td>8:00 pm</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td>7:00 am</td>
            <td></td>
            <td></td>
            <td>7:00 am</td>
            <td></td>
            <td>8:00 am</td>
        </tr>
        </tbody>
    </table>
</div>


<h1>Kick Boxing Deportivo</h1>
<p>Es un arte marcial de contacto de origen japonés en el cual se mezclan las técnicas de lucha o combate del boxeo, el karate Kyokushinkai   y el boxeo thailandes.</p>

<h1>Horarios</h1>

<h4>Kick Boxing Deportivo:</h4>



<div class="container">
<table class="table table-hover">
        <thead>
        <tr>
            <th>OPCIÓN</th>
            <th>LUNES</th>
            <th>MARTES</th>
            <th>MIÉRCOLES</th>
            <th>JUEVES</th>
            <th>VIERNES</th>
            <th>SABADO</th>
            <th>DOMINGO</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td></td>
                <td>6:30 pm</td>
                <td></td>
                <td>6:30 pm</td>
                <td></td>
                <td>2:00 pm</td>
                <td></td>
            </tr>
            <tr>
                <td>2</td>
                <td></td>
                <td>7:00 am</td>
                <td></td>
                <td></td>
                <td>7:00 am</td>
                <td></td>
                <td>10:00 am</td>
            </tr>
        </tbody>
    </table>
</div>







