<?php


/*
  Controller name: Gestor de usuarios (GU)
  Controller description: Gestionar usuarios
  Controller Author: Julián Donado Barriga
    
*/

GLOBAL $wpdb;

if(isset($_GET['metodo'])) {
    $metodo=$_GET['metodo'];
    if($metodo=='ingresar_usuario'){
      ingresar_usuario();
    }elseif($metodo=='mensualidad'){
      mensualidad();
    }elseif($metodo=='matricula'){
      matricula();
    }elseif($metodo=='consultar_miusuario'){
      consultar_miusuario();
    }elseif($metodo=='agendar_fisio'){
      agendar_fisio();
    }elseif($metodo=='crear_agendar'){
      crear_agendar();
    }elseif($metodo=='datosMedicos'){
      datosMedicos();
    }elseif($metodo=='enrolamiento'){
      enrolamiento();
    }elseif($metodo=='edit_datosMedicos'){
      edit_datosMedicos();
    }elseif($metodo=='enrolamientoLinea'){
      enrolamientoLinea();
    }elseif($metodo=='editer_user'){
      editer_user();
    }elseif($metodo=='edit_paquete'){
      edit_paquete();
    }elseif($metodo=='new_paquete'){
      new_paquete();
    }elseif($metodo=='activar_paquete'){
      activar_paquete();
    }elseif($metodo=='mensualidadxusuario'){
      mensualidadxusuario();
    }elseif($metodo=='edit_usuario_mensualidad'){
      edit_usuario_mensualidad();
    }

}

function agendar_fisio(){

  GLOBAL $wpdb;

  if(isset($_POST['fechahora1']) && isset($_POST['estado'])){
    $fecha1=$_POST['fechahora1'];
    $estado=$_POST['estado'];

    $opt1=$wpdb->prefix . "gu_fdates";


    $wpdb->insert(
      $opt1,
      array(
        'fd_opcion' =>$fecha1,
        'f_activo' =>$activar1
          )
      );

      echo' <script>alert("Estás activo en nuestra base de datos, ahora podrás agendar tu cita");window.location.replace("http://localhost/expofighting/administracion-de-fisioterapia/");</script>';

          
  }


  
}


function mensualidad(){

  GLOBAL $wpdb;  

  if(isset($_POST['new_value'])){

      $new_precio=$_POST['new_value'];
      

          
      $tabla_precio= $wpdb->prefix. 'gu_mensualidad';

      // print_r($new_precio);
      // die();


    $inactivar_precios=$wpdb->update(
      $tabla_precio,
    array(

          "m_estado"=>2
          
        ),
        array(
          "m_estado"=>1
        )

    );

    if($inactivar_precios==true){

      $wpdb->insert(
        $tabla_precio,
          array(
            "m_valor"=>$new_precio,
            "m_estado"=>1
            
          )
                
        );
    
        echo '<script>window.location.replace("http://localhost/expofighting/?page_id=71"); alert ("El valor de la mensualidad se ha actualizado de forma satisgactoria.")</script>';

    }
     
     
    } else{
      echo '<script>window.location.replace("http://localhost/expofighting/?page_id=71"); alert ("No se pudo realizar el cambio de la mensualidad, intentelo mas tarde , si el problema persiste contacte al desarrollador.")</script>';
    } 
    
}

function matricula(){
  GLOBAL $wpdb;

  if(isset($_POST['new_matricula'])){
    $nueva_matricula=$_POST['new_matricula'];

        $tabla_matricula=$wpdb->prefix. 'gu_matricula';
    
        $inactivar_anterior= $wpdb->update(

          $tabla_matricula,
        array(
    
          "ma_estado"=>2
        ),
        array(
          "ma_estado"=>1
        )
    
        );


        if($inactivar_anterior==true){

          $wpdb->insert(
            $tabla_matricula,
              array(
                "ma_valor"=>$nueva_matricula,
                "ma_estado"=>1
                
              )
                    
            );

   
  
            echo '<script>window.location.replace("http://localhost/expofighting/?page_id=71"); alert ("El valor de la matrícula se ha actualizado de forma satisgactoria.")</script>';
          
        }else{
         
        echo '<script>window.location.replace("http://localhost/expofighting/?page_id=71"); alert ("No se pudo realizar el cambio de la matricula, intentelo mas tarde. Si el problema persiste contacte al desarrollador.")</script>';        
      
        } 

    }

   
}

function mensualidadxusuario(){

  GLOBAL $wpdb;

  if(isset($_POST['pqt']) && isset($_POST['estado_pago']) && isset($_POST['usuario']) ){

    $paquete=$_POST['pqt'];
    $estado_pago=$_POST['estado_pago'];
    $user=$_POST['usuario'];

    
    if($paquete==2){

      $bd_usuaxpago=$wpdb->prefix. 'gu_usuarioxpago';
      $matricula="SELECT * FROM ".$wpdb->prefix. "gu_matricula WHERE ma_estado=1";
      $result_matricula=$wpdb->get_results($matricula);
      $valor= "SELECT * FROM ".$wpdb->prefix. "gu_mensualidad WHERE m_estado=1";
      $mensualidad_actual=$wpdb->get_results($valor);

      foreach($result_matricula as $matri_pqt2){

        $valor_matricula=$matri_pqt2->ma_valor;

      }
//0.5455
      foreach($mensualidad_actual as $mensualidad_pqt2){

        $descuento=0.7727272727272727;

        $valor_mensual= $mensualidad_pqt2->m_valor*(2);
        $mensualidad_pqt2= ($valor_mensual)*‬*($descuento);
      }  

      $wpdb->insert(
        $bd_usuaxpago,
        array(
          "u_pk"=>$user,
          "pq_pk"=>$paquete,
          "uxp_valor_matricula"=>$valor_matricula,
          "uxp_valor_mensual"=>$mensualidad_pqt2,
          "e_pk"=>$estado_pago
          )
        );


        print_r($valor_mensual);
        echo $wpdb->last_query;
        echo $wpdb->last_result;
        echo $wpdb->last_error;
        die();


        echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("Los datos de pago se han creado correctamente. paquete 2")</script>';
    }else{
      
      echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("No se ha podido actualizar los datos de pago, intentelo mas tarde o comuniquese con el desarrollador. paquete 2")</script>';

   
    } 

      if($paquete==1){

            $bd_usuaxpago=$wpdb->prefix. 'gu_usuarioxpago';
            $matricula="SELECT * FROM ".$wpdb->prefix. "gu_matricula WHERE ma_estado=1";
            $result_matricula=$wpdb->get_results($matricula);
            $valor= "SELECT * FROM ".$wpdb->prefix. "gu_mensualidad WHERE m_estado=1";
            $mensualidad_actual=$wpdb->get_results($valor);

            foreach($result_matricula as $matri_pqt1){

              $valor_matricula=$matri_pqt1->ma_valor*(0.5);


            }

            foreach($mensualidad_actual as $mensualidad_pqt1){

              $valor_mensual=$mensualidad_pqt1->m_valor;
            }  

            $wpdb->insert(
              $bd_usuaxpago,
              array(
                "u_pk"=>$user,
                "pq_pk"=>$paquete,
                "uxp_valor_matricula"=>$valor_matricula,
                "uxp_valor_mensual"=>$valor_mensual,
                "e_pk"=>$estado_pago
                )
              );


              echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("Los datos de pago se han creado correctamente paquete 1.")</script>';
      }else{
        
              echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("No se ha podido actualizar los datos de pago, intentelo mas tarde o comuniquese con el desarrollador paquete 1")</script>';
        
      }
      
      
      


  }


}


function edit_usuario_mensualidad(){

GLOBAL $wpdb;

  if(isset($_POST['nuevo_valor']) && isset($_POST['usuario'])){

    $nueva_mensualidad=$_POST['nuevo_valor'];
    $usuario_linea=$_POST['usuario'];

    $bd_usuaxpago=$wpdb->prefix. 'gu_usuarioxpago';

    $wpdb->update(
      $bd_usuaxpago,
    array(

          "uxp_valor_mensual"=>$nueva_mensualidad          
        ),
        array(
          "u_pk"=>$usuario_linea
        ),
        array(
          '%s'
        )

    );

    echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("Se ha actualizado la mensualidad exitosamente.")</script>';

  }else{
    echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("No se ha podido actualizar los datos, intente nuevamente o comuniquese con el desarrollador")</script>';
  }

}


      



function consultar_miusuario(){

  GLOBAL $wpdb; 


  if(isset($_POST['usuario'])){

    $consultar_user=$_POST['usuario'];

    $tabla_user=$wpdb->prefix. 'gu_usuario';

    $buscar_usuario="SELECT * FROM ".$tabla_user."  WHERE u_documento=".$consultar_user. " and e_pk=1";

    $result_user=$wpdb->get_results($buscar_usuario);

      if(sizeof($result_user)>0){

        echo' <script>alert("Estás activo en nuestra base de datos, ahora podrás agendar tu cita");window.location.replace("http://localhost/expofighting/aparta-tu-cita/");</script>';

      }else{
        echo' <script>alert("No te encontramos en nuestra base de datos, por favor comunicate con EXPOFIGHTING para validar tu estado");window.location.replace("http://localhost/expofighting/fisioterapia/");</script>';
        
      }

         

  }
}

function editer_user(){

   

  if(isset($_POST['person_doc']) || isset($_POST['person_name']) || isset($_POST['person_lstname']) || isset($_POST['person_email'])){
    
   
    
    $p_doc=$_POST['person_doc'];
    $p_name=$_POST['person_name'];
    $p_Lname=$_POST['person_lstname'];
    $p_mail=$_POST['person_email'];

  GLOBAL $wpdb;

   $buscar_usuario="SELECT * FROM wp_gu_usuario WHERE u_documento= '".$p_doc."' OR u_correo_e= '".$p_mail."' OR u_nombre LIKE ('%".$p_name."%') OR u_apellido LIKE ('%".$p_Lname."%')";


   $result_buscar_usuario=$wpdb->get_results($buscar_usuario);


  //  print_r($result_buscar_usuario);
  //  die();

  require_once(PLUGIN_GU_HOME.'/views/usuarios/editar_usuario.php');

  //  echo' <script>window.location.replace("http://localhost/expofighting/actualizar-usuario/");</script>';

   
  }
  
}


/*function agendar_fisio(){

  GLOBAL $wpdb;

  if(isset($_POST['fechahora'])){
    $cita_fecha=$_POST['fechahora'];

    $consultar_user;

    $tabla_cita= $wpdb->prefix . "gu_fisiatria";
    
    $wpdb->insert(
      $tabla_cita,

      array(
        'f_fecha_cita'=>$cita_fecha,
        'u_pk'=>$consultar_user
        
      )
      );


  }
}*/





// Formulario ingresar nuevo usuario

function ingresar_usuario(){

GLOBAL $wpdb;


 if(isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['num_doc']) && isset($_POST['tipo_doc']) && isset($_POST['nacido']) && isset($_POST['celular']) && isset($_POST['fijo']) && isset($_POST['mail']) && isset($_POST['direcc']) && isset($_POST['pais']) && isset($_POST['ciudad']) ){
  

  $Nombre= $_POST['nombre'];
  $Apellido= $_POST['apellido'];
  $Ndoc=$_POST['num_doc'];
  $Tdoc=$_POST{'tipo_doc'};
  $fnacimiento=$_POST['nacido'];
  $Tcel=$_POST['celular'];
  $Tfijo=$_POST['fijo'];
  $correo=$_POST['mail'];
  $residencia=$_POST['direcc'];
  $Pais=$_POST['pais'];
  $ciudad=$_POST['ciudad'];
  

  GLOBAL $wpdb;

$TABLA_USUARIOS=$wpdb->prefix . 'gu_usuario';
$validar_usuario="select u_documento from ".$wpdb->prefix . 'gu_usuario WHERE u_documento='.$Ndoc;
$result_validar_usuario=$wpdb->get_results($validar_usuario);

// print_r($result_validar_usuario);

if($result_validar_usuario==$Ndoc){
  
  echo '<script>window.location.replace("http://localhost/expofighting/buscar-usuarios/"); alert ("El usuario ya se encuentra creado, por favor verifique su disponibilidad y actualice sus datos.")</script>';

  }else{

    $wpdb->insert(
      $TABLA_USUARIOS,
        array(
          "u_nombre"=>$Nombre,
          "u_apellido"=>$Apellido,
          "u_documento"=>$Ndoc,
          "td_tipo_doc"=>$Tdoc,
          "u_fecha_nacimiento"=>$fnacimiento,
          "u_celular"=>$Tcel,
          "u_fijo"=>$Tfijo,
          "u_correo_e"=>$correo,
          "u_direccion"=>$residencia,
          "e_pk"=>1
          
        )
              
      );
  
      echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("Felicitaciones,el usuario se ha creado de forma correcta.")</script>';

   
  
  }
    
};



}

function datosMedicos(){

  if(isset($_POST['eps']) && isset($_POST['g_sangre']) && isset($_POST['anota_med']) && isset($_POST['peso']) && isset($_POST['estatura']) && isset($_POST['usuario']) && isset($_POST['preg1']) && isset($_POST['preg2']) && isset($_POST['preg3']) && isset($_POST['preg4']) && isset($_POST['preg5']) && isset($_POST['preg6']) && isset($_POST['preg7'])){

  $Eps=$_POST{'eps'};
  $Gsangui=$_POST['g_sangre'];
  $notas_med=$_POST['anota_med'];
  $peso=$_POST['peso'];
  $estatura=$_POST['estatura'];
  $usuario_linea=$_POST['usuario'];
  $pregunta1=$_POST['preg1'];
  $pregunta2=$_POST['preg2'];
  $pregunta3=$_POST['preg3'];
  $pregunta4=$_POST['preg4'];
  $pregunta5=$_POST['preg5'];
  $pregunta6=$_POST['preg6'];
  $pregunta7=$_POST['preg7'];


  GLOBAL $wpdb;


  $datos_medicos=$wpdb->prefix. "gu_datos_medicos WHERE U_PK=".$usuario_linea;
  
    if(sizeof($datos_medicos)>0){

      echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("El usuario ya tiene los datos médicos agregados, si lo requeire, por favor edítelos por la interfaz.")</script>';

    }else{

        $wpdb->insert(
          $datos_medicos,
            array(
                "eps_pk"=>$Eps,
                "gs_pk"=>$Gsangui,
                "dm_nota_med"=>$notas_med,
                "dm_peso"=>$peso,
                "dm_estatura"=>$estatura,
                "u_pk"=>$usuario_linea,
                "parq1"=>$pregunta1,
                "parq2"=>$pregunta2,
                "parq3"=>$pregunta3,
                "parq4"=>$pregunta4,
                "parq5"=>$pregunta5,
                "parq6"=>$pregunta6,
                "parq7"=>$pregunta7
                
                )
            );
       
  
        echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("Los datos médicos se han creado de forma correcta.")</script>';


      }

      
      
  }else{
    echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("Fallo la creación de los datos médicos, intentelo de nuevo mas tarde o comuniquese con el desarrollador.")</script>';
  }
}

function edit_datosMedicos(){

  if(isset($_POST['eps']) && isset($_POST['g_sangre']) && isset($_POST['anota_med']) && isset($_POST['peso']) && isset($_POST['estatura']) && isset($_POST['usuario'])){

    $Eps=$_POST{'eps'};
    $Gsangui=$_POST['g_sangre'];
    $notas_med=$_POST['anota_med'];
    $peso=$_POST['peso'];
    $estatura=$_POST['estatura'];
    $usuario_linea=$_POST['usuario'];
  
  
    GLOBAL $wpdb;
  
  
    $edit_datos_medicos=$wpdb->prefix. "gu_datos_medicos";

    $wpdb->update(
      $edit_datos_medicos,
    array(

          "eps_pk"=>$Eps,
          "gs_pk"=>$Gsangui,
          "dm_nota_med"=>$notas_med,
          "dm_peso"=>$peso,
          "dm_estatura"=>$estatura
          
        ),
        array(
          "u_pk"=>$usuario_linea
        ),
        array(
          '%s'
        )

    );

    echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("Los datos médicos se han actualizado de forma correcta en la base de datos.")</script>';

    // 
  }

  

 

}


function enrolamiento(){
  if(isset($_POST['prog']) && isset($_POST['rol']) && isset($_POST['usuario'])){

    $prog=$_POST['prog'];
    $role=$_POST['rol'];
    $user_linea=$_POST['usuario'];

    // Print_r($user_linea);
    // die();

    GLOBAL $wpdb;

    $enroll=$wpdb->prefix. "gu_enrolamiento";

    $wpdb->insert(
      $enroll,
      array(
        "p_pk"=>$prog,
        "r_pk"=>$role,
        "u_pk"=>$user_linea,
        "e_pk"=>1
      )
      );


      echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("Los datos del programa se han crado correctamente")</script>';
      
  }else{
    echo '<script>window.location.replace("http://localhost/expofighting/ver-usuario-creado/"); alert ("Fallo la creación de los datos de programa, intentelo de nuevo mas tarde o comuniquese con el desarrollador.")</script>';
  }
}


function new_paquete(){

  if(isset($_POST['nombre_pqt']) && isset($_POST['description']) && isset($_POST['comentario']) && isset($_POST['estado_paquete']) && isset($_POST['id_paquete'])){

    $nombre_paquete=$_POST['nombre_pqt'];
    $desc=$_POST['description'];
    $comment=$_POST['comentario'];
    $estado=$_POST['estado_paquete'];
    $id_paq=$_POST['id_paquete'];

    GLOBAL $wpdb;

    $tabla_paquetes=$wpdb->prefix. "gu_paquetes";

    $validar_paquete="SELECT * FROM ".$wpdb->prefix. "gu_paquetes WHERE pq_id=".$id_paq." OR pq_nombre = ". $nombre_paquete ;

    $result_validar_paquete=$wpdb->get_results($validar_paquete);

      if(sizeof($result_validar_paquete)>0){

        echo '<script>window.location.replace("http://localhost/expofighting/?page_id=71"); alert ("El paquete no se ha podido crear ya que el ID ya existe en la base de datos, por favor revise y cree mas tarde o comuniquese con el desarrollador si lo ve necesario.")</script>';
        

      }else{

        $wpdb->insert(
          $tabla_paquetes,
          array(
            "pq_nombre"=>$nombre_paquete,
            "pq_descripción"=>$desc,
            "pq_comentario_interno"=>$comment,
            "e_pk"=>$estado,
            "pq_id"=>$id_paq
            )
          );

        echo '<script>window.location.replace("http://localhost/expofighting/?page_id=71"); alert ("El paquete se ha crado correctamente")</script>';
      }

  }else{
    
    echo '<script>window.location.replace("http://localhost/expofighting/?page_id=71"); alert ("Se ha generado un error, intente mas tarde o comuniquese con el desarrollador.")</script>';
  }
}


function edit_paquete(){

  

  if(isset($_POST['pk_paquete']) || isset($_POST['new_description']) || isset($_POST['nuevo_estado']) || isset($_POST['new_comentario']) || isset($_POST['pq_nombre']) || isset($_POST['cambiar_estado']) ){

    $identificador=$_POST['pk_paquete'];
    $nueva_desc=$_POST['new_description'];
    $nuevo_estado=$_POST['cambiar_estado'];
    $nevo_comentario=$_POST['new_comentario'];
    $new_name=$_POST['pq_nombre'];
    $activar_pq=$_POST['activar'];
    
// print_r($new_name);
// die();
  
    GLOBAL $wpdb;

    $tabla_paquetes=$wpdb->prefix. "gu_paquetes";

          if (!empty($nueva_desc)){

            $wpdb->update(
              $tabla_paquetes,
            array(

                  "pq_descripción"=>$nueva_desc
                ),
                array(
                  "pq_pk"=>$identificador
                ),
                array(
                  '%s'
                )

            );
          
          }elseif(!empty($nevo_comentario)){
            
              $wpdb->update(
                $tabla_paquetes,
              array(
  
                    "pq_comentario_interno"=>$nevo_comentario
                  ),
                  array(
                    "pq_pk"=>$identificador
                  ),
                  array(
                    '%s'
                  )
  
              );
          }elseif(!empty($new_name)){

            $wpdb->update(
              $tabla_paquetes,
            array(

                  "pq_nombre"=>$new_name
                ),
                array(
                  "pq_pk"=>$identificador
                ),
                array(
                  '%s'
                )

            );

          }elseif(!empty($nuevo_estado)){

            $wpdb->update(
              $tabla_paquetes,
            array(

                  "e_pk"=>$nuevo_estado
                ),
                array(
                  "pq_pk"=>$identificador
                ),
                array(
                  '%s'
                )

            );
          }else{
      echo '<script>window.location.replace("http://localhost/expofighting/?page_id=71"); alert ("Fallo la modificación de los datos de paquete, intentelo de nuevo mas tarde o comuniquese con el desarrollador.")</script>';
  }

}
}


function activar_paquete(){

  $activar_pq=array();

  if(isset($_POST['activar'])){

    $activar_pq=$_POST['activar'];
    

    GLOBAL $wpdb;

    $tabla_paquetes=$wpdb->prefix. "gu_paquetes";

    if(!empty($activar_pq)){

      foreach($activar_pq as $selected){

          $wpdb->update(
    
            $tabla_paquetes,
            array(
                  "e_pk"=>1
                ),
                array(
                  "pq_pk"=>$selected
                ),
                array(
                  '%s'
                )
    
            );
      
    }
    echo '<script>window.location.replace("http://localhost/expofighting/?page_id=71"); alert ("El o los paquetes se han actualizado de forma correcta.")</script>';

    }else{
      echo '<script>window.location.replace("http://localhost/expofighting/?page_id=71"); alert ("El paquete no ha podido ser activado, intentelo mas tarde o comuniquese con el desarrollador.")</script>';

  }
     
    
  }
}



function my_index(){
GLOBAL $wpdb;

    require_once(PLUGIN_GU_HOME.'/views/index.php');
}

function func_nosotros(){

  require_once(PLUGIN_GU_HOME.'/views/nosotros.php');
}

function func_opinion(){

  require_once(PLUGIN_GU_HOME.'/views/tuopinionvale.php');
}


function func_profesores(){

  require_once(PLUGIN_GU_HOME.'./views/profesores/profesores.php');
}

function func_kickboxing(){

  require_once(PLUGIN_GU_HOME.'./views/kickboxing.php');
}

function func_taekwondo(){

  require_once(PLUGIN_GU_HOME.'./views/taekwondo.php');
}

function func_exp_motriz(){

  require_once(PLUGIN_GU_HOME.'./views/exp_motriz.php');
}

function func_musica(){

  require_once(PLUGIN_GU_HOME.'./views/musica.php');
}

function func_listar_usuarios(){

  GLOBAL $wpdb;

  $user_active= "SELECT * FROM wp_gu_usuario u
  INNER JOIN wp_gu_estado es ON u.e_pk=es.e_pk
  INNER JOIN wp_gu_programa p on u.p_pk=p.p_pk ";

//recordar incluir esta linea cuando se finalice el desarrollo del pago mensual --INNER JOIN wp_gu_usuarioxpago uxp ON u.u_pk=uxp.u_pk

  $result_user_active=$wpdb->get_results($user_active);


  require_once(PLUGIN_GU_HOME.'./views/usuarios/listar_usuarios.php');
}

function func_dashboard(){

  GLOBAL $wpdb;

  /**
   * 
   * Mensualidad y matrícula
   * 
   */

  $valor_Actual="SELECT ma_valor FROM ".$wpdb->prefix. "gu_matricula WHERE ma_estado=1";

  $result_valor_Actual=$wpdb->get_results($valor_Actual);

  $valor= "SELECT m_valor FROM ".$wpdb->prefix. "gu_mensualidad WHERE m_estado=1";
  $mensualidad_actual=$wpdb->get_results($valor);

  $valor_Actual="SELECT ma_valor FROM ".$wpdb->prefix. "gu_matricula WHERE ma_estado=1";

  $result_valor_Actual=$wpdb->get_results($valor_Actual);

  $user_active= "SELECT * FROM wp_gu_usuario";

  $result_user_active=$wpdb->get_results($user_active);

  // print_r($result_active);

  // echo $wpdb->last_query;
  //     echo $wpdb->last_result;
  //     echo $wpdb->last_error;
  //     // die();

/**
   * 
   * fin Mensualidad y matrícula
   * 
   */

  $ver_programas="SELECT * FROM ".$wpdb->prefix. "gu_programas ";
  
  $results_ver_programas= $wpdb->get_results($ver_programas);

  $todos_programas="SELECT * FROM wp_gu_enrolamiento e 
  INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk 
  INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk 
  INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk 
  INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk 
  WHERE es.e_pk=1  ORDER BY p.p_pk ASC";

  $result_todos_programas=$wpdb->get_results($todos_programas);

  $all_programasKB="SELECT * FROM wp_gu_enrolamiento e INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE es.e_pk=1 and p.p_pk in (1,2) ORDER BY p.p_pk ASC";

  $result_all_programasKB=$wpdb->get_results($all_programasKB);

  $all_programastk="SELECT * FROM wp_gu_enrolamiento e INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE es.e_pk=1 and p.p_pk in (3,4) ORDER BY p.p_pk ASC";

  $result_all_programastk=$wpdb->get_results($all_programastk);

  $all_programasex="SELECT * FROM wp_gu_enrolamiento e INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE es.e_pk=1 and p.p_pk in (5) ORDER BY p.p_pk ASC";

  $result_all_programasex=$wpdb->get_results($all_programasex);

  $all_programasmusic="SELECT * FROM wp_gu_enrolamiento e INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE es.e_pk=1 and p.p_pk in (7,8) ORDER BY p.p_pk ASC";

  $result_all_programasmusic=$wpdb->get_results($all_programasmusic);

  $kb_deportivo="SELECT COUNT(u.u_pk) FROM wp_gu_enrolamiento e INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE p.p_pk=1 and es.e_pk=1";

  $result_kb_deportivo=$wpdb->get_results($kb_deportivo);

  $kb_ejecutivo="SELECT * FROM wp_gu_enrolamiento e 
  INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk 
  INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk 
  INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk 
  INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk 
  WHERE p.p_pk=2 and es.e_pk=1";

  $result_kb_ejecutivo=$wpdb->get_results($kb_ejecutivo);

  $tk_inf="SELECT COUNT(u.u_pk) FROM wp_gu_enrolamiento e INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE p.p_pk=4 and es.e_pk=1";

  $result_tk_inf=$wpdb->get_results($tk_inf);
  
  $tk_deportivo="SELECT COUNT(u.u_pk) FROM wp_gu_enrolamiento e INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE p.p_pk=3 and es.e_pk=1";

  $result_tk_deportivo=$wpdb->get_results($tk_deportivo);

  $exploracion="SELECT COUNT(u.u_pk) FROM wp_gu_enrolamiento e INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE p.p_pk=5 and es.e_pk=1";

  $result_exploracion=$wpdb->get_results($exploracion);

  $music_ini="SELECT COUNT(u.u_pk) FROM wp_gu_enrolamiento e INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE p.p_pk=7 and es.e_pk=1";

  $result_music_ini=$wpdb->get_results($music_ini);

  $music_musica="SELECT COUNT(u.u_pk) FROM wp_gu_enrolamiento e INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE p.p_pk=8 and es.e_pk=1";

  $result_music_musica=$wpdb->get_results($music_musica);

  // $mes_ini=date("yy m");
  // $mes_end=date("yy m");
  $detalle_usuario="SELECT * FROM wp_gu_usuario WHERE e_pk=1 
  
  -- AND u_fecha_nacimiento BETWEEN "//.$mes_ini
  ;

  // print_r($mes_actual);
  // die();

  $result_detalle_usuario=$wpdb->get_results($detalle_usuario);

  

  $estados="SELECT * FROM wp_gu_estado ";
  $result_estados=$wpdb->get_results($estados);
  
  $paquetes="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE e.e_pk=2";
  $result_paquetes=$wpdb->get_results($paquetes);

  $paquete1="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=1 AND e.e_pk=1";
  $result_paquete1=$wpdb->get_results($paquete1);

  // print_r($paquete1);
  // die();

  $paquete2="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=2 AND e.e_pk=1";
  $result_paquete2=$wpdb->get_results($paquete2);

  $paquete3="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=3 AND e.e_pk=1";
  $result_paquete3=$wpdb->get_results($paquete3);

  $paquete4="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=4 AND e.e_pk=1";
  $result_paquete4=$wpdb->get_results($paquete4);

  $paquete5="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=5 AND e.e_pk=1";
  $result_paquete5=$wpdb->get_results($paquete5);

  $paquete6="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=6 AND e.e_pk=1";
  $result_paquete6=$wpdb->get_results($paquete6);

  $paquete7="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=7 AND e.e_pk=1";
  $result_paquete7=$wpdb->get_results($paquete7);

  $paquete8="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=8 AND e.e_pk=1";
  $result_paquete8=$wpdb->get_results($paquete8);

  require_once(PLUGIN_GU_HOME.'./views/dashboard.php');
}

function func_ingresar_usuario(){
GLOBAL $wpdb;

  $tipo_doc="SELECT * FROM " .$wpdb->prefix. "gu_tipo_doc";
  $result_tipodoc=$wpdb->get_results($tipo_doc);

  

  
  require_once(PLUGIN_GU_HOME.'./views/usuarios/inscribir_usuarios.php');
}

function mostrar_usuario_creado(){
  GLOBAL $wpdb;

  $valor_Actual="SELECT ma_valor FROM ".$wpdb->prefix. "gu_matricula WHERE ma_estado=1";

  $result_valor_Actual=$wpdb->get_results($valor_Actual);

  $valor= "SELECT m_valor FROM ".$wpdb->prefix. "gu_mensualidad WHERE m_estado=1";
  $mensualidad_actual=$wpdb->get_results($valor);
  

  $estado="SELECT * FROM wp_gu_estado WHERE e_id IN (5,6)";

  $result_estado=$wpdb->get_results($estado);
////////////////////////////////////////////////
  $eps="SELECT * FROM " .$wpdb->prefix. "gu_eps";
   $result_eps=$wpdb->get_results($eps);

  $gsanguineo="SELECT * FROM ".$wpdb->prefix. "gu_grupo_sanguíneo";
  $result_gsanguineo=$wpdb->get_results($gsanguineo);


  ////////////////////////////////////////////////
 $ultimo_usuario_registrado="SELECT * FROM  ".$wpdb->prefix. "gu_usuario  u
 INNER JOIN wp_gu_tipo_doc t ON t.td_pk=u.td_tipo_doc ORDER BY u.u_fecha_creacion DESC limit 1";


 $result_ultimo_usuario_registrado=$wpdb->get_results($ultimo_usuario_registrado);


////////////////////////////////////////////////
$borndate=$result_ultimo_usuario_registrado->u_fecha_nacimiento;
 $y_nacimiento="SELECT YEAR($borndate) FROM  ".$wpdb->prefix. "gu_usuario";

 $result_y_nacimiento=$wpdb->get_results($y_nacimiento);
 ////////////////////////////////////////////////

 foreach($result_ultimo_usuario_registrado as $ver_pk_usuario){

$pk_user=$ver_pk_usuario->u_pk;

$ver_dato_medico="SELECT * FROM  wp_gu_datos_medicos dm
INNER JOIN wp_gu_eps eps ON eps.eps_pk=dm.eps_pk
INNER JOIN wp_gu_grupo_sanguíneo gs ON gs.gs_pk=dm.gs_pk
INNER JOIN wp_gu_usuario u ON u.u_pk=dm.u_pk WHERE dm.u_pk=".$pk_user;

$result_ver_dato_medico=$wpdb->get_results($ver_dato_medico);

////

$last_enrol="SELECT  * FROM wp_gu_enrolamiento e
INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk
INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk
INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk
INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE e.u_pk=".$pk_user;

$result_last_enrol=$wpdb->get_results($last_enrol);

//    echo $wpdb->last_query;
// echo $wpdb->last_result;
// echo $wpdb->last_error;

}

/////////////////////////////////////////////////////
$programa="SELECT * FROM ".$wpdb->prefix. "gu_programa";
$result_prog=$wpdb->get_results($programa);

////////////////////////////////////////////////////

$role="SELECT * FROM ".$wpdb->prefix. "gu_role";
$result_role=$wpdb->get_results($role);
//////////////////////////////////////////////////


$paquetes="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE e.e_pk=1";

  $result_paquetes=$wpdb->get_results($paquetes);

  $paquete1="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=1 AND e.e_pk=1";
  $result_paquete1=$wpdb->get_results($paquete1);

  // print_r($paquete1);
  // die();

  $paquete2="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=2 AND e.e_pk=1";
  $result_paquete2=$wpdb->get_results($paquete2);

  $paquete3="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=3 AND e.e_pk=1";
  $result_paquete3=$wpdb->get_results($paquete3);

  $paquete4="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=4 AND e.e_pk=1";
  $result_paquete4=$wpdb->get_results($paquete4);

  $paquete5="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=5 AND e.e_pk=1";
  $result_paquete5=$wpdb->get_results($paquete5);

  $paquete6="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=6 AND e.e_pk=1";
  $result_paquete6=$wpdb->get_results($paquete6);

  $paquete7="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=7 AND e.e_pk=1";
  $result_paquete7=$wpdb->get_results($paquete7);

  $paquete8="SELECT * FROM wp_gu_paquetes pq
  INNER JOIN wp_gu_estado e ON pq.e_pk=e.e_pk
  WHERE pq.PQ_ID=8 AND e.e_pk=1";
  $result_paquete8=$wpdb->get_results($paquete8);

  $ver_pago="SELECT * FROM wp_gu_usuarioxpago uxp
  INNER JOIN wp_gu_usuario u ON u.u_pk=uxp.u_pk
  INNER JOIN wp_gu_estado es ON es.e_pk=uxp.e_pk
  INNER JOIN wp_gu_paquetes pqt ON uxp.pq_pk=pqt.pq_pk";

  $result_ver_pago=$wpdb->get_results($ver_pago);

// die();

  require_once(PLUGIN_GU_HOME.'./views/usuarios/ver_usuario_creado.php');
}


function enrolamientoLinea($pkenrolamiento){

  $linea_enrol="SELECT  * FROM wp_gu_enrolamiento e
INNER JOIN wp_gu_usuario u ON e.u_pk=u.u_pk
INNER JOIN wp_gu_programa p ON p.p_pk=e.p_pk
INNER JOIN wp_gu_estado es ON es.e_pk=e.e_pk
INNER JOIN wp_gu_role r ON r.rol_pk=e.r_pk WHERE e.er_pk=".$pkenrolamiento;

$rsult_linea_enrol=$wpdb->get_results($linea_enrol);


require_once(PLUGIN_GU_HOME.'./views/usuarios/ver_linea_enroll.php');


}


function func_editar_usuario(){
  GLOBAL $wpdb;

  
  require_once(PLUGIN_GU_HOME.'./views/usuarios/editar_usuario.php');
}

function ver_func_editar_usuario(){
  GLOBAL $wpdb;

  
  require_once(PLUGIN_GU_HOME.'./views/usuarios/ver_editar_usuario.php');

}



function func_programas(){
GLOBAL $wpdb;


  require_once(PLUGIN_GU_HOME.'./views/programas.php');
}

function func_admin(){
  require_once(PLUGIN_GU_HOME.'./views/admin.php');

}

function func_carlos(){
  require_once(PLUGIN_GU_HOME.'./views/profesores/carlos.php');

}

function func_jairo(){
  require_once(PLUGIN_GU_HOME.'./views/profesores/jairo.php');

}

function func_daniela(){
  require_once(PLUGIN_GU_HOME.'./views/profesores/daniela.php');

}

function func_marcelo(){
  require_once(PLUGIN_GU_HOME.'./views/profesores/marcelo.php');

}


function func_ernesto(){
  require_once(PLUGIN_GU_HOME.'./views/profesores/ernesto.php');

}

function fuc_calendario_fisio(){
GLOBAL $wpdb;

$tipoCita="SELECT FROM ".$wpdb->prefix. "gu_citas_fisiatria WHERE cf_id_cita=1";


  require_once(PLUGIN_GU_HOME.'./views/fisioterapia/calendario.php');

}

function func_fisioterapia(){

  require_once(PLUGIN_GU_HOME.'./views/fisioterapia/fisioterapia.php');
}

function func_admin_fisio(){
  
  
  require_once(PLUGIN_GU_HOME.'./views/fisioterapia/admin_fisio.php');

}


?>
