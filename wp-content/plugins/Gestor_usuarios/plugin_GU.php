<?php

/*

  Plugin Name: Gestor de usuarios (GU)

  Description: Este plugin ayudará a la gestión de usuarios como datos básicos, programas, estado de matricula y demás.
  
  Version: 1.1

  Author: Julián Donado Barriga


  License: GU_001

 */

define('JAU_VERSION', '2.5');

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

define('PLUGIN_GU_HOME', dirname(__FILE__));

GLOBAL $wpdb;

include_once (PLUGIN_GU_HOME."/controller/controlador.php");

register_activation_hook( PLUGIN_GU_HOME."/db-installer.php", 'PLUGIN_GU_DB_install' );

require_once(PLUGIN_GU_HOME."/db-installer.php");

add_action( 'plugins_loaded', 'PLUGIN_GU_DB_install' );

add_shortcode( 'index','my_index' );

add_shortcode( 'nuevo_usuario','mostrar_crear_usuarios');

add_shortcode( 'ver_usuario_creado','mostrar_usuario_creado');

add_shortcode( 'nosotros','func_nosotros');

add_shortcode( 'opinion','func_opinion' );

add_shortcode( 'profesores','func_profesores' );

add_shortcode( 'kickboxing','func_kickboxing' );

add_shortcode( 'taekwondo','func_taekwondo' );

add_shortcode( 'exp_motriz','func_exp_motriz' );

add_shortcode( 'musica','func_musica' );

add_shortcode( 'dashboard','func_dashboard' );

add_shortcode( 'ingresar_usuario','func_ingresar_usuario' );

add_shortcode( 'editar_usuario','func_editar_usuario' );

add_shortcode( 'ver_editar_usuario','ver_func_editar_usuario' );

// add_shortcode( 'mensualidad','func_mensualidad' );

add_shortcode( 'fisioterapia','func_fisioterapia' );

add_shortcode( 'programas','func_programas' );

// add_shortcode( 'admin','func_admin' );
 
add_shortcode( 'carlos','func_carlos' );

add_shortcode( 'jairo','func_jairo' );

add_shortcode( 'daniela','func_daniela' );

add_shortcode( 'marcelo','func_marcelo' );

add_shortcode( 'ernesto','func_ernesto' );

add_shortcode( 'calendario_fisio','fuc_calendario_fisio');

add_shortcode( 'admin_fisio','func_admin_fisio' );

// add_shortcode( 'admin_matricula','func_admin_matricula' );

add_shortcode( 'listar_usuarios','func_listar_usuarios' );
  

?>


