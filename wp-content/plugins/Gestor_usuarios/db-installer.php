<?php

function PLUGIN_GU_DB_install() {

	GLOBAL $wpdb;

	$charset_collate = $wpdb->get_charset_collate();

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	/***
	 * Nombre de tabla: tipo documento
	 * Descripción:  Tabla para crear tipos de documento (CC,Ti. etc)
	 * 
	 */

	$table_tipo_doc = $wpdb->prefix . 'gu_tipo_doc';
	
	$sql1 = "CREATE TABLE $table_tipo_doc (		
		td_pk INT (5) NOT NULL AUTO_INCREMENT,
		td_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		td_nombre VARCHAR (30) NOT NULL,
		td_abrev VARCHAR (3) NOT NULL,
		
		UNIQUE KEY gu_tipo_doc_id (td_pk)

	) $charset_collate;";
	
	dbDelta( $sql1 );

	/***
	 * Nombre de tabla: Estado
	 * Descripción:  Tabla donde se identificaran estados como matriculado, activo, inactivo etc.
	 * 
	 */

	$table_estado = $wpdb->prefix . 'gu_estado';

	$sql2 = "CREATE TABLE $table_doc (
		
		e_pk INT (5) NOT NULL AUTO_INCREMENT,
		e_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		e_nombre VARCHAR (20) NOT NULL	,	
		e_id INT (5) NOT NULL,
		e_letra VARCHAR (5) NOT NULL,
		
		UNIQUE KEY gu_estado_id (e_pk)

	) $charset_collate;";

	dbDelta( $sql2 );

	/***
	 * Nombre de tabla: programas
	 * Descripción:  Tabla para listar los programas actuales e históricos de la escuela.
	 * 
	 */

	$programa= $wpdb->prefix . 'gu_programa';

	$sql3 ="CREATE TABLE $programa (

		p_pk INT (5) NOT NULL AUTO_INCREMENT,
		p_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		p_nombre VARCHAR (40) NOT NULL,	
		p_id INT (3) NOT NULL,
		
		UNIQUE KEY gu_programa_id (p_pk)

	) $charset_collate;";

	dbDelta( $sql3 );

	/***
	 * Nombre de tabla: paquetes
	 * Descripción:  Tabla para crear los paquetes de servicios ofrecidos por la escuela-
	 * Nota: incluir paquetes de # de personas, matricula, 
	 * 
	 */

	$paquetes= $wpdb->prefix . 'gu_paquetes';

	$sql4 ="CREATE TABLE $paquetes (

		pq_pk INT (5) NOT NULL AUTO_INCREMENT,
		pq_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		pq_nombre VARCHAR (50) NOT NULL	,
		pq_descripción	text COLLATE utf8_bin NOT NULL,	
		pq_id INT (3) NOT NULL,
		e_pk INT (10) NOT NULL,
		
		UNIQUE KEY gu_paquetes_id (pq_pk)

	) $charset_collate;";

	dbDelta( $sql4 );

	/***
	 * Nombre de tabla: Mensualidad
	 * Descripción:  Tabla para asignar el valor de la mensualidad
	 * 
	 */

	$mensualidad= $wpdb->prefix . 'gu_mensualidad';

	$sql5 ="CREATE TABLE $mensualidad (

		m_pk INT (5) NOT NULL AUTO_INCREMENT,
		m_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		m_valor INT (10) NOT NULL,
		m_estado INT (5) NOT NULL,
		
		UNIQUE KEY gu_mensualidad_id (m_pk)

	) $charset_collate;";

	dbDelta( $sql5 );

	
	/***
	 * Nombre de tabla: usuarios
	 * Descripción:  Tabla proncipar donde se alojará la información del matriculado
	 * 
	 */

	$user= $wpdb->prefix . 'gu_usuario';

	$sql6 ="CREATE TABLE $user (
		u_pk INT (10) NOT NULL AUTO_INCREMENT,
		u_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		u_nombre VARCHAR (30) NOT NULL,
		u_apellido VARCHAR (30) NOT NULL,
		u_documento INT (30) NOT NULL,
		td_tipo_doc INT (5) NOT NULL, 
		u_fecha_nacimiento DATE ,
		u_celular INT (20) NOT NULL,
		u_fijo INT (20) NULL,
		u_direccion VARCHAR (100) NOT NULL,
		u_correo_e VARCHAR (30) NOT NULL,
		u_pais VARCHAR (30) NOT NULL,
		u_ciudad VARCHAR (30) NOT NULL,
		dm_pk INT (10) NOT NULL,
		p_pk INT (5) NOT NULL, 
		e_pk INT (5) NOT NULL,
		u_acu_pk INT (20) NULL ,
		
		UNIQUE KEY gu_usuario_id (u_pk)

	) $charset_collate;";
	

	dbDelta( $sql6 );

	/***
	 * Nombre de tabla: Datos médicos
	 * Descripción:  Tabla para proporcionar datos médicos relevantes
	 * 
	 */

	$datos_medico= $wpdb->prefix . 'gu_datos_medicos';

	$sql11 ="CREATE TABLE $datos_medico (
		dm_pk INT (10) NOT NULL AUTO_INCREMENT,
		dm_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		u_pk INT (10) NOT NULL,
		dm_nota_med  TEXT NOT NULL,
		dm_peso INT (5) NOT NULL,
		dm_estatura INT (5) NOT NULL,
		eps_pk INT (30) NOT NULL,
		gs_pk INT (10) NOT NULL,
		
		UNIQUE KEY gu_datos_medicos_id (dm_pk)

	) $charset_collate;";
	

	dbDelta( $sql11 );

	/***
	 * Nombre de tabla: Mensualidad
	 * Descripción:  Tabla para asignar el valor de la mensualidad
	 * 
	 */

	$eps= $wpdb->prefix . 'gu_eps';

	$sql7 ="CREATE TABLE $eps (

		eps_pk INT (5) NOT NULL AUTO_INCREMENT,
		eps_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		eps_nombre VARCHAR (40) NOT NULL,
		eps_id INT (10) NOT NULL,
		
		UNIQUE KEY gu_eps_id (eps_pk)

	) $charset_collate;";

	dbDelta( $sql7 );



	/***
	 * Nombre de tabla: Grupo sanguineo
	 * Descripción:  Tabla con la opciones del grupoy RH
	 * 
	 */

	$rh= $wpdb->prefix . 'gu_grupo_sanguíneo';

	$sql8 ="CREATE TABLE $rh (

		gs_pk INT (5) NOT NULL AUTO_INCREMENT,
		gs_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		gs_nombre VARCHAR (10) NOT NULL,
		gs_id INT (10) NOT NULL,
		
		UNIQUE KEY gu_grupo_sanguíneo_id (gs_pk)

	) $charset_collate;";

	dbDelta( $sql8 );



	/***
	 * Nombre de tabla: 
	 * Descripción:  Tabla con la opciones del grupoy RH
	 * 
	 */

	$fisio= $wpdb->prefix . 'gu_fisiatría';

	$sql9= "CREATE TABLE $fisio (
		f_pk INT (5) NOT NULL AUTO_INCREMENT,
		f_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		u_pk INT (10) NOT NULL,
		cf_pk INT (10)  NOT NULL
		f_fecha_cita DATE (10) NOT NULL

		UNIQUE KEY gu_fisiatría_id (f_pk)

		) $charset_collate;";

dbDelta( $sql9 );
		
		
/***
	 * Nombre de tabla: 
	 * Descripción:  Tabla con la opciones del grupoy RH
	 * 
	 */

	$citasFisio= $wpdb->prefix . 'gu_citas_fisiatria';

	$sql10 ="CREATE TABLE $citasFisio (

		cf_pk INT (10) NOT NULL AUTO_INCREMENT,
		cf_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		cf_tipo_cita VARCHAR (10) NOT NULL,
		cf_id_cita INT (10) NOT NULL,
		
		UNIQUE KEY gu_citas_fisiatria_id (cf_pk)

	) $charset_collate;";

	dbDelta( $sql10 );


	/***
	 * Nombre de tabla: 
	 * Descripción:  Tabla con la opciones del grupoy RH
	 * 
	 */

	$enroll= $wpdb->prefix . 'gu_enrolamiento';

	$sql12 ="CREATE TABLE $enroll (

		er_pk INT (10) NOT NULL AUTO_INCREMENT,
		er_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		u_pk INT (10) NOT NULL,---PK DEL USUARIO
		p_pk INT (10) NOT NULL,---PK DEL PROGRAMA
		e_pk INT (10) NOT NULL,--ESTADO DEL ENROLAMEINTO
		r_pk INT (10) NOT NULL, --Role del usuario
		
		UNIQUE KEY gu_enrolamiento_id (er_pk)

	) $charset_collate;";

	//   echo $wpdb->last_query;
    //   echo $wpdb->last_result;
	//   echo $wpdb->last_error;
	//   die();

	dbDelta( $sql12 );

	/***
	 * Nombre de tabla: 
	 * Descripción:  Tabla con la opciones del grupoy RH
	 * 
	 */

	$roll= $wpdb->prefix . 'gu_role';

	$sql13 ="CREATE TABLE $roll (

		rol_pk INT (10) NOT NULL AUTO_INCREMENT,
		rol_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		u_pk INT (10) NOT NULL,
		rol_NOMBRE VARCHAR (20) NOT NULL,
		
		UNIQUE KEY gu_role_id (rol_pk)

	) $charset_collate;";

	dbDelta( $sql13 );

/***
	 * Nombre de tabla: usuarioxpago
	 * Descripción:  Tabla donde se registrarán los pagos y fechas de vencimiento por ususario
	 * 
	 * tablas foraneas: u_pk (usuarios), pq_pk (paquetes), m_pk (mensualidad), ma_pk (matricula), e_pk (estado)
	 * 
	 */
	$userxpago=$wpdb->prefix . 'gu_usuarioxpago';

	$sql14 ="CREATE TABLE $userxpago (
		uxp_pk INT (10) NOT NULL AUTO_INCREMENT,
		uxp_fecha_creacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
		u_pk INT (10) NOT NULL,
		pq_pk INT (10) NOT NULL,
		uxp_valor_matricula INT (10) NOT NULL, 
		uxp_valor_mensual int (10) NOT NULL,
		e_pk INT (10) NOT NULL,
		
		
		
		UNIQUE KEY gu_usuarioxpago_id (uxp_pk)

	) $charset_collate;";

	dbDelta( $sql14 );


	
}

?>