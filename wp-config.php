<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'expofighting' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*MvS#c3)Rx`KCM2sDOSJ+n#jxeD%},!3KHc3&2^!pJ&*upC#_yx9za,*=dw@*`Wv' );
define( 'SECURE_AUTH_KEY',  'ucI94<GLKN[=jt2ezNeU<m-Bj5k1yd4,qN*1!{c@pT~`7&(0M$!&[elvpC{K~Z%@' );
define( 'LOGGED_IN_KEY',    ' Nvv5i.[Pl^Jp+&;E<k4 5%Fe#rH3p$]5O|8!5RM:I!!AOF~@psV9yAKoltxDTYg' );
define( 'NONCE_KEY',        'xW{?a`H}VRPu<L)VU*#krKDc 0/#5eZHI#Z&B@Ar1BM,JxSw1sfJWI1S0KeY/]y=' );
define( 'AUTH_SALT',        'V&9rQArwRf 6-fDwd#l^;5C!RS:@tXObI<n!Ylk_ DMU^ak7@sf@iX@8,)ZnpvA:' );
define( 'SECURE_AUTH_SALT', '-9>yr(,n]J<%crS`7emybx|<F](^SJDNY$.tE]1r^C{3=d=isP)!P9J *#,@s8~p' );
define( 'LOGGED_IN_SALT',   '9m*66[hdc2k,EK^p#,5h!9rw_`9=[XpF1O@V$>:9+2V5$Y//X#wrX|?y/9uO_is^' );
define( 'NONCE_SALT',       '5k?eOi8BZa!pBN,mhg[#IAcqYk6)/WQdyD4J,6~z`+WG*x0UOjaJ;v.-CKP$f;3{' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
